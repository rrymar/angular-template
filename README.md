# AppTemplate

## Instalation
Install VS 2019

Install MS SQL 2016 or higher

Install Node 10
`https://nodejs.org/dist/v10.16.0/node-v10.16.0-x64.msi`

Install Angular CLI 
`npm install -g @angular/cli`

## .Net Development

Check connection string in app.settings (also check tests).
Build and Run Tests

Create new Db with database\MigrationsScript.sql script

Use scripts\create-ef-migration.ps1 to create new migration.
If you create manual migration run scripts\update-migration-script.ps1 to update MigrationsScript.sql 

## UI Development 

Go to AppTemplate.UI folder 

Change environment.apiUrl

Run `npm i` to install deps

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

