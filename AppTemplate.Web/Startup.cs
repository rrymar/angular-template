using AppTemplate.Database;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.Swagger;
using Autofac;
using Web.Core;
using Web.Core.Errors;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Web.Core.Authentication;
using AppTemplate.Web.Features.Authentication;
using System;
using AppTemplate.Web.Features.Users;
using System.Collections.Generic;
using Microsoft.ApplicationInsights.Extensibility;
using Web.Core.User;

namespace AppTemplate.Web
{
    public class Startup
    {
        private readonly string AllowDevServerOrigins = "allowDevServer";

        public const string MainDbConnection = "MainDb";

        public Startup(IHostingEnvironment env)
        {
            Configuration = env.AddAppSettings();
        }

        public IConfiguration Configuration { get; }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddDatabase<DataContext>(Configuration.GetConnectionString(MainDbConnection)
                , "AppTemplate.Database");

            services.AddCors(options =>
            {
                options.AddPolicy(AllowDevServerOrigins,
                builder => builder.WithOrigins("http://localhost:4200")
                .AllowAnyHeader().AllowAnyMethod());
            });

            AddSwagger(services);

            services.AddSingleton<ITelemetryInitializer, AppInsightsInitializer>();
            services.AddApplicationInsightsTelemetry();

            AddMvc(services);

            services.AddJWTAuthentication(Configuration);
            services.AddEmails(Configuration);

            services.AddHttpContextAccessor();

            var autofac = services.AddAutofac();
            RegisterModules(autofac);
            RegisterServices(autofac);

            var container = autofac.Build();
            return container.Resolve<IServiceProvider>();
        }

        private static void AddSwagger(IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "AppTemplate API", Version = "V1" });

                c.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    Description = "JWT Authorization header. Example: \"Bearer {token}\"",
                    Name = "Authorization",
                    In = "header",
                    Type = "apiKey"
                });

                var security = new Dictionary<string, IEnumerable<string>>
                {
                    {"Bearer", new string[] { }},
                };

                c.AddSecurityRequirement(security);
            });
        }

        protected virtual IMvcBuilder AddMvc(IServiceCollection services)
        {
            return services.AddMvc(options =>
            {
                var policy = new AuthorizationPolicyBuilder()
                              .RequireAuthenticatedUser()
                              .Build();

                options.Filters.Add(new AuthorizeFilter(policy));
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        public virtual void RegisterServices(ContainerBuilder builder)
        {
        }

        private void RegisterModules(ContainerBuilder autofac)
        {
            autofac.RegisterModule<WebCoreModule>();
            autofac.RegisterModule<AuthenticationModule>();
            autofac.RegisterModule<UsersModule>();
        }

        public virtual void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
            }

            app.UseCors(AllowDevServerOrigins);

            app.UseMiddleware<ErrorHandlingMiddleware>();

            app.UseAuthentication();

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "AppTemplate API V1"));
            loggerFactory.AddSerilog(Configuration, MainDbConnection);
        }
    }
}
