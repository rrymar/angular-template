﻿namespace AppTemplate.Web.Features.Authentication.CurrentUser
{
    public class CurrentUserModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public bool IsAdmin { get; set; }
    }
}
