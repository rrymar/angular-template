﻿using AppTemplate.Database.Entities.Users;
using Microsoft.AspNetCore.Mvc;
using Web.Core.User;

namespace AppTemplate.Web.Features.Authentication.CurrentUser
{
    [Route(Urls.Authentication.CurrentUser)]
    public class CurrentUserController : Controller
    {
        private readonly ICurrentUserContext currentUser;

        public CurrentUserController(ICurrentUserContext currentUser)
        {
            this.currentUser = currentUser;
        }

        [HttpGet]
        public CurrentUserModel Get()
        {
            return new CurrentUserModel()
            {
                Id = currentUser.UserId,
                Name = currentUser.GetFullName(),
                IsAdmin = currentUser.HasRole(KnownRoles.SystemAdministrator),
            };
        }
    }
}
