﻿using AppTemplate.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Core.User;

namespace AppTemplate.Web.Features.Authentication
{
    public class CurrentUserLocator : ICurrentUserLocator
    {
        private readonly ICurrentUserContext currentUserContext;

        public CurrentUserLocator(ICurrentUserContext currentUserContext)
        {
            this.currentUserContext = currentUserContext;
        }

        public int UserId => currentUserContext.UserId;
    }
}
