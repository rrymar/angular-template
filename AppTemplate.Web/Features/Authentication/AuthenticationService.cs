﻿using AppTemplate.Database;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using Web.Core;
using Web.Core.Authentication;
using Web.Core.Errors;
using Web.Core.User;

namespace AppTemplate.Web.Features.Authentication
{
    public class AuthenticationService
    {
        private readonly DataContext context;

        private readonly HashService hashService;

        private readonly JwtOptions config;

        public AuthenticationService(DataContext context,
            HashService hashService,
            IOptions<JwtOptions> config)
        {
            this.context = context;
            this.hashService = hashService;
            this.config = config.Value;
        }

        public string BuildToken(UserPrincipal user)
        {
            var creds = config.GetSigningCredentials();
            var issuer = config.Issuer;

            var token = new JwtSecurityToken(issuer, issuer, user.Claims,
              expires: DateTime.Now.AddHours(config.ExpireInHours),
              signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        public UserPrincipal Authenticate(string userName, string password)
        {
            var user = context.Users.Where(u => u.Username == userName && u.IsActive)
                .Include(u => u.Roles).ThenInclude(r => r.Role).SingleOrDefault();

            if (user == null || !hashService.ValidateHash(password, user.PasswordHash, user.PasswordSalt))
                throw new UnauthorizedException();

            var roles = user.Roles.Where(r => r.IsActive).Select(r => r.Role.Name);
            return new UserPrincipal(user.Id, user.Username, user.FullName, roles);
        }
    }
}
