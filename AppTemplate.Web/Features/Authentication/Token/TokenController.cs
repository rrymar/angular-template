using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTemplate.Web.Features.Authentication.Token
{
    [Route(Urls.Authentication.Token)]
    public class TokenController : Controller
    {
        private readonly AuthenticationService authenticationService;

        public TokenController(AuthenticationService authenticationService)
        {
            this.authenticationService = authenticationService;
        }

        [HttpPost, AllowAnonymous]
        public IActionResult CreateToken([FromBody]LoginModel login)
        {
            IActionResult response = Unauthorized();

            var user = authenticationService.Authenticate(login.Username, login.Password);

            if (user != null)
            {
                var tokenString = authenticationService.BuildToken(user);
                return Ok(new TokenModel() { Token = tokenString });
            }

            return response;
        }
    }
}
