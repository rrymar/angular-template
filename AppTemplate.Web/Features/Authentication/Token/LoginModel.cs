﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTemplate.Web.Features.Authentication.Token
{
    public class LoginModel
    {
        public string Username { get; set; }

        public string Password { get; set; }
    }
}
