﻿using AppTemplate.Database;
using AppTemplate.Web.Features.Authentication.ResetPassword;
using Autofac;

namespace AppTemplate.Web.Features.Authentication
{
    public class AuthenticationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AuthenticationService>().AsSelf();
            builder.RegisterType<ResetPasswordService>().AsSelf();
            builder.RegisterType<CurrentUserLocator>().As<ICurrentUserLocator>();
        }
    }
}
