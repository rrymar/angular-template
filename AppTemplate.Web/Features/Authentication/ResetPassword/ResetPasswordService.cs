﻿using AppTemplate.Database;
using AppTemplate.Database.Entities.Users;
using Microsoft.Extensions.Options;
using System;
using System.Linq;
using Web.Core;
using Web.Core.Email;
using Web.Core.Errors;

namespace AppTemplate.Web.Features.Authentication.ResetPassword
{
    public class ResetPasswordService
    {
        private readonly DataContext context;

        private readonly HashService hashService;

        private readonly IEmailSender emailSender;

        private readonly EmailOptions options;

        public ResetPasswordService(DataContext context,
            HashService hashService,
            IEmailSender emailSender, IOptions<EmailOptions> options)
        {
            this.context = context;
            this.hashService = hashService;
            this.emailSender = emailSender;

            if (options.Value == null)
                throw new ApplicationException("Please specify email options in app.settings.");

            this.options = options.Value;
        }

        private const string ResetPasswordEmail = "Please use this link to reset password {{ link }}";

        private const string ResetPasswordSubject = "Reset Password Request";

        public void RequestResetPassword(ResetPasswordRequestModel model)
        {
            var user = context.Users.Where(u => u.Username == model.Username && u.IsActive)
                .SingleOrDefault();

            if (user == null || string.IsNullOrWhiteSpace(user.Email)) return;

            var request = new ResetPasswordRequest()
            {
                UserId = user.Id,
                Secret = Guid.NewGuid()
            };
            context.ResetPasswordRequests.Add(request);
            context.SaveChanges();

            var link = $"{options.UiRoot}public/reset-password/{request.Id}?secret={request.Secret}";
            emailSender.Send(new EmailModel()
            {
                Subject = ResetPasswordSubject,
                Template = ResetPasswordEmail,
                Model = new { link },
                ToEmail = user.Email
            });
        }

        private const int ExpirationInMin = 15;

        public void ResetPassword(ResetPasswordModel model)
        {
            var request = context.ResetPasswordRequests.Find(model.RequestId);
            if (request == null) return;

            if(!request.IsActive || (DateTime.UtcNow - request.CreatedOn).TotalMinutes > ExpirationInMin)
                throw new BusinessValidationException("Reset pasword request has already expired");

            var user = context.Users.Find(request.UserId);
            if (user?.IsActive != true) return;

            if (string.IsNullOrWhiteSpace(model.Password) || model.Password?.Length < 5)
                throw new BusinessValidationException("Password should contain more then 5 symbols");

            request.IsActive = false;

            var (hash, salt) = hashService.CreateHash(model.Password);
            user.PasswordHash = hash;
            user.PasswordSalt = salt;

            context.SaveChanges();
        }
    }
}
