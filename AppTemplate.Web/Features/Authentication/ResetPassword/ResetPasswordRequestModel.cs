﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AppTemplate.Web.Features.Authentication.ResetPassword
{
    public class ResetPasswordRequestModel
    {
        public string Username { get; set; }
    }
}
