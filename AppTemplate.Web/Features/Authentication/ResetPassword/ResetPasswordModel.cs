﻿using System;

namespace AppTemplate.Web.Features.Authentication.ResetPassword
{
    public class ResetPasswordModel
    {
        public int RequestId { get; set; }

        public Guid Secret { get; set; }

        public string Password { get; set; }
    }
}
