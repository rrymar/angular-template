using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AppTemplate.Web.Features.Authentication.ResetPassword
{
    [Route(Urls.Authentication.ResetPassword)]
    public class ResetPasswordController : Controller
    {
        private readonly ResetPasswordService service;

        public ResetPasswordController(ResetPasswordService service)
        {
            this.service = service;
        }

        [HttpPost, AllowAnonymous]
        public IActionResult ResetPassword([FromBody]ResetPasswordModel model)
        {
            service.ResetPassword(model);
            return Ok();
        }
    }
}
