using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AppTemplate.Web.Features.Authentication.ResetPassword
{
    [Route(Urls.Authentication.ResetPasswordRequest)]
    public class ResetPasswordRequestController : Controller
    {
        private readonly ResetPasswordService service;

        public ResetPasswordRequestController(ResetPasswordService service)
        {
            this.service = service;
        }

        [HttpPost, AllowAnonymous]
        public IActionResult ResetPassword([FromBody]ResetPasswordRequestModel model)
        {
            service.RequestResetPassword(model);
            return Ok();
        }
    }
}
