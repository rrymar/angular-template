﻿using AppTemplate.Database.Entities.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Web.Core.CRUD;

namespace AppTemplate.Web.Features.Users
{
    [Route(Urls.Users)]
    [Authorize(Roles = KnownRoles.SystemAdministrator)]
    public class UsersController : CrudControler<UserModel, SearchQuery>
    {
        public UsersController(ICrudService<UserModel, SearchQuery> service) : base(service)
        {
        }
    }
}
