﻿using AppTemplate.Database;
using AppTemplate.Database.Entities.Users;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Web.Core;
using Web.Core.CRUD;
using Web.Core.Errors;
using AppTemplate.Database.Extensions;

namespace AppTemplate.Web.Features.Users
{
    public class UsersService : ICrudService<UserModel, SearchQuery>
    {
        private readonly DataContext context;

        private readonly HashService hashService;

        public UsersService(DataContext context, HashService hashService)
        {
            this.context = context;
            this.hashService = hashService;
        }

        public UserModel Get(int id)
        {
            return ToModel(GetUser(id));
        }

        private User GetUser(int id)
        {
            var user = context.Users.Find(id);
            if (user?.IsActive != true)
                throw new NotFoundException();

            return user;
        }

        public ResultsList<UserModel> Get(SearchQuery query)
        {
            var baseQuery = context.Users.Include(u => u.Roles)
                .FilterActiveOnly(query.ActiveOnly);

            var count = baseQuery.Count();
            if (count <= 0)
                return new ResultsList<UserModel>();

            var items = baseQuery.ApplyQuery(query)
                .Select(ToModel).ToList();

            return new ResultsList<UserModel>(items, count);
        }

        private UserModel ToModel(User e)
        {
            return new UserModel()
            {
                Id = e.Id,
                IsActive = e.IsActive,
                FirstName = e.FirstName,
                LastName = e.LastName,
                Username = e.Username,
                CreatedOn = e.CreatedOn,
                UpdatedOn = e.UpdatedOn,
                FullName = e.FullName,
                Email = e.Email
            };
        }

        public UserModel Create(UserModel model)
        {
            ValidateModel(model);
            ValidatePassword(model);

            if (context.Users.Any(u => u.Username == model.Username))
                throw new BusinessValidationException("User name already taken");

            var (hash, salt) = hashService.CreateHash(model.Password);

            var user = new User()
            {
                Username = model.Username,
                PasswordHash = hash,
                PasswordSalt = salt,
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email
            };

            context.Users.Add(user);
            context.SaveChanges();

            return ToModel(user);
        }

        private void ValidateModel(UserModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Username))
                throw new BusinessValidationException("User name shouldn't be empty");

            if (string.IsNullOrWhiteSpace(model.Email))
                throw new BusinessValidationException("Email shouldn't be empty");

            if (context.Users.Any(u => u.Email == model.Email && u.Id != model.Id))
                throw new BusinessValidationException("User name already taken");
        }

        public UserModel Update(int id, UserModel model)
        {
            ValidateModel(model);

            var user = GetUser(id);

            if (!string.IsNullOrEmpty(model.Password))
            {
                ValidatePassword(model);

                var (hash, salt) = hashService.CreateHash(model.Password);

                user.PasswordHash = hash;
                user.PasswordSalt = salt;
            }

            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.Email = model.Email;

            context.SaveChanges();

            return ToModel(user);
        }

        private static void ValidatePassword(UserModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Password) || model.Password?.Length < 5)
                throw new BusinessValidationException("Password should contain more then 5 symbols");
        }

        public void Delete(int id)
        {
            var user = context.Users.Find(id);

            if (user == null)
                throw new NotFoundException();

            user.IsActive = false;
            context.SaveChanges();
        }
    }
}
