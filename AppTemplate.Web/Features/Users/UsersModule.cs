using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Web.Core.CRUD;

namespace AppTemplate.Web.Features.Users
{
    public class UsersModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<UsersService>().As<ICrudService<UserModel, SearchQuery>>();
        }
    }
}
