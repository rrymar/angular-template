namespace AppTemplate.Web
{
    public static class Urls
    {
        public const string Base = "api";

        public static class Authentication
        {
            public const string Base = Urls.Base + "/Authentication";

            public const string Token = Base + "/Token";

            public const string ResetPassword = Base + "/ResetPassword";

            public const string ResetPasswordRequest = Base + "/ResetPasswordRequest";

            public const string CurrentUser = Base + "/CurrentUser";
        }

        public const string Users = Base + "/Users";
    }
}
