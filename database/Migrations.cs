using System.Reflection;

namespace database.migrations
{
    public static class Migrations
    {
        public static Assembly Assembly => typeof(Migrations).Assembly;
    }
}