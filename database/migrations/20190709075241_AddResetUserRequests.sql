IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190709075241_AddResetUserRequests')
BEGIN
    CREATE TABLE [ResetPasswordRequests] (
        [Id] int NOT NULL IDENTITY,
        [IsActive] bit NOT NULL,
        [CreatedOn] datetime2 NOT NULL DEFAULT (GETUTCDATE()),
        [UpdatedOn] datetime2 NOT NULL DEFAULT (GETUTCDATE()),
        [UserId] int NOT NULL,
		[Secret] uniqueidentifier NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
        CONSTRAINT [PK_ResetPasswordRequests] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_ResetPasswordRequests_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190709075241_AddResetUserRequests')
BEGIN
    CREATE INDEX [IX_ResetPasswordRequests_IsActive] ON [ResetPasswordRequests] ([IsActive]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190709075241_AddResetUserRequests')
BEGIN
    CREATE INDEX [IX_ResetPasswordRequests_UserId] ON [ResetPasswordRequests] ([UserId]);
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190709075241_AddResetUserRequests')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20190709075241_AddResetUserRequests', N'2.2.4-servicing-10062');
END;

GO

