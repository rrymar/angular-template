IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190626071917_AddUsersAndRoles_Data')
BEGIN
    INSERT INTO [dbo].[Roles] ([Name])
    VALUES ('SystemAdministrator');

    INSERT INTO [dbo].[Users] 
           ([Username]
           ,[PasswordHash]
		   ,[PasswordSalt]
           ,[FirstName]
           ,[LastName]
           ,[IsActive])
    VALUES ('admin'
           ,N'kls7C9YCw3Eh1hZo/oUqtBf61kTntDcQHQil9Nr9D1/vy3UFkJI0AgWJjjH5qpjI2htuoAy3l/ZjhZ8CpUH2FQ=='
		   ,N'/u95H8PMiwO/tZkJj/CbNasI2Gs4NMqR'
           ,'Admin'
           ,'Admin'
           ,1);
    
    INSERT INTO [dbo].[UserRoles] ([UserId], [RoleId], [IsActive])
    VALUES (1, 1, 1)
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190626071917_AddUsersAndRoles_Data')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20190626071917_AddUsersAndRoles_Data', N'2.2.4-servicing-10062');
END;

GO

