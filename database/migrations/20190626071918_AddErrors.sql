IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190626071918_AddErrors')
BEGIN
    CREATE TABLE [Errors] (
        [Id] int NOT NULL IDENTITY,
        [Message] nvarchar(max) NULL,
        [Level] nvarchar(128) NULL,
        [TimeStamp] datetime NOT NULL,
        [Exception] nvarchar(max) NULL,
        [RequestUri] nvarchar(300) NULL,
        [UserName] nvarchar(50) NULL,
        CONSTRAINT [PK_Errors] PRIMARY KEY ([Id])
    );
END;

GO

IF NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190626071918_AddErrors')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20190626071918_AddErrors', N'2.2.4-servicing-10062');
END;

GO

