IF DB_ID('AppTemplate') IS NULL
	CREATE DATABASE AppTemplate;
GO

USE AppTemplate;

IF OBJECT_ID(N'__EFMigrationsHistory') IS NULL
BEGIN
	CREATE TABLE [__EFMigrationsHistory] (
		[MigrationId] nvarchar(150) NOT NULL,
		[ProductVersion] nvarchar(32) NOT NULL,
		CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
	);
END;

SET ANSI_PADDING ON;
SET XACT_ABORT ON;
BEGIN TRANSACTION

IF @@TRANCOUNT>0 AND NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190626070722_Initial')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20190626070722_Initial', N'2.2.4-servicing-10062');
END;

GO

IF @@TRANCOUNT>0 AND NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190626071916_AddUsersAndRoles')
BEGIN
    CREATE TABLE [Roles] (
        [Id] int NOT NULL IDENTITY,
        [Name] nvarchar(30) NULL,
        CONSTRAINT [PK_Roles] PRIMARY KEY ([Id])
    );
END;

GO

IF @@TRANCOUNT>0 AND NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190626071916_AddUsersAndRoles')
BEGIN
    CREATE TABLE [Users] (
        [Id] int NOT NULL IDENTITY,
        [IsActive] bit NOT NULL,
        [CreatedOn] datetime2 NOT NULL DEFAULT (GETUTCDATE()),
        [UpdatedOn] datetime2 NOT NULL DEFAULT (GETUTCDATE()),
        [Username] nvarchar(32) NULL,
        [PasswordHash] nvarchar(100) NULL,
		[PasswordSalt] nvarchar(40) NULL,
        [FirstName] nvarchar(50) NULL,
        [LastName] nvarchar(50) NULL,
        [FullName] AS CONCAT(FirstName,' ', LastName),
		[Email] nvarchar(200) NULL,
        CONSTRAINT [PK_Users] PRIMARY KEY ([Id])
    );
END;

GO

IF @@TRANCOUNT>0 AND NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190626071916_AddUsersAndRoles')
BEGIN
    CREATE TABLE [UserRoles] (
        [Id] int NOT NULL IDENTITY,
        [IsActive] bit NOT NULL,
        [CreatedOn] datetime2 NOT NULL DEFAULT (GETUTCDATE()),
        [UpdatedOn] datetime2 NOT NULL DEFAULT (GETUTCDATE()),
        [UserId] int NOT NULL,
        [RoleId] int NOT NULL,
        CONSTRAINT [PK_UserRoles] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_UserRoles_Roles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [Roles] ([Id]) ON DELETE NO ACTION,
        CONSTRAINT [FK_UserRoles_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
    );
END;

GO

IF @@TRANCOUNT>0 AND NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190626071916_AddUsersAndRoles')
BEGIN
    CREATE INDEX [IX_UserRoles_IsActive] ON [UserRoles] ([IsActive]);
END;

GO

IF @@TRANCOUNT>0 AND NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190626071916_AddUsersAndRoles')
BEGIN
    CREATE INDEX [IX_UserRoles_RoleId] ON [UserRoles] ([RoleId]);
END;

GO

IF @@TRANCOUNT>0 AND NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190626071916_AddUsersAndRoles')
BEGIN
    CREATE INDEX [IX_UserRoles_UserId] ON [UserRoles] ([UserId]);
END;

GO

IF @@TRANCOUNT>0 AND NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190626071916_AddUsersAndRoles')
BEGIN
    CREATE INDEX [IX_Users_IsActive] ON [Users] ([IsActive]);
END;

GO

IF @@TRANCOUNT>0 AND NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190626071916_AddUsersAndRoles')
BEGIN
    CREATE INDEX [IX_Users_Username] ON [Users] ([Username]);
END;

GO

IF @@TRANCOUNT>0 AND NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190626071916_AddUsersAndRoles')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20190626071916_AddUsersAndRoles', N'2.2.4-servicing-10062');
END;

GO

IF @@TRANCOUNT>0 AND NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190626071917_AddUsersAndRoles_Data')
BEGIN
    INSERT INTO [dbo].[Roles] ([Name])
    VALUES ('SystemAdministrator');

    INSERT INTO [dbo].[Users] 
           ([Username]
           ,[PasswordHash]
		   ,[PasswordSalt]
           ,[FirstName]
           ,[LastName]
           ,[IsActive])
    VALUES ('admin'
           ,N'kls7C9YCw3Eh1hZo/oUqtBf61kTntDcQHQil9Nr9D1/vy3UFkJI0AgWJjjH5qpjI2htuoAy3l/ZjhZ8CpUH2FQ=='
		   ,N'/u95H8PMiwO/tZkJj/CbNasI2Gs4NMqR'
           ,'Admin'
           ,'Admin'
           ,1);
    
    INSERT INTO [dbo].[UserRoles] ([UserId], [RoleId], [IsActive])
    VALUES (1, 1, 1)
END;

GO

IF @@TRANCOUNT>0 AND NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190626071917_AddUsersAndRoles_Data')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20190626071917_AddUsersAndRoles_Data', N'2.2.4-servicing-10062');
END;

GO

IF @@TRANCOUNT>0 AND NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190626071918_AddErrors')
BEGIN
    CREATE TABLE [Errors] (
        [Id] int NOT NULL IDENTITY,
        [Message] nvarchar(max) NULL,
        [Level] nvarchar(128) NULL,
        [TimeStamp] datetime NOT NULL,
        [Exception] nvarchar(max) NULL,
        [RequestUri] nvarchar(300) NULL,
        [UserName] nvarchar(50) NULL,
        CONSTRAINT [PK_Errors] PRIMARY KEY ([Id])
    );
END;

GO

IF @@TRANCOUNT>0 AND NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190626071918_AddErrors')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20190626071918_AddErrors', N'2.2.4-servicing-10062');
END;

GO

IF @@TRANCOUNT>0 AND NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190709075241_AddResetUserRequests')
BEGIN
    CREATE TABLE [ResetPasswordRequests] (
        [Id] int NOT NULL IDENTITY,
        [IsActive] bit NOT NULL,
        [CreatedOn] datetime2 NOT NULL DEFAULT (GETUTCDATE()),
        [UpdatedOn] datetime2 NOT NULL DEFAULT (GETUTCDATE()),
        [UserId] int NOT NULL,
		[Secret] uniqueidentifier NOT NULL DEFAULT '00000000-0000-0000-0000-000000000000',
        CONSTRAINT [PK_ResetPasswordRequests] PRIMARY KEY ([Id]),
        CONSTRAINT [FK_ResetPasswordRequests_Users_UserId] FOREIGN KEY ([UserId]) REFERENCES [Users] ([Id]) ON DELETE NO ACTION
    );
END;

GO

IF @@TRANCOUNT>0 AND NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190709075241_AddResetUserRequests')
BEGIN
    CREATE INDEX [IX_ResetPasswordRequests_IsActive] ON [ResetPasswordRequests] ([IsActive]);
END;

GO

IF @@TRANCOUNT>0 AND NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190709075241_AddResetUserRequests')
BEGIN
    CREATE INDEX [IX_ResetPasswordRequests_UserId] ON [ResetPasswordRequests] ([UserId]);
END;

GO

IF @@TRANCOUNT>0 AND NOT EXISTS(SELECT * FROM [__EFMigrationsHistory] WHERE [MigrationId] = N'20190709075241_AddResetUserRequests')
BEGIN
    INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
    VALUES (N'20190709075241_AddResetUserRequests', N'2.2.4-servicing-10062');
END;

GO

IF @@TRANCOUNT > 0
	COMMIT TRANSACTION
ELSE
	PRINT 'Transaction should be rolled back by XACT_ABORT property'
GO

