﻿using Microsoft.EntityFrameworkCore;

namespace Tests.Core.Db
{
    public interface ITestMigration<T>
         where T : DbContext
    {
        string Name { get; }

        void Execute(T context);
    }
}
