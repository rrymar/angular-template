﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;

namespace Tests.Core.Db
{
    public class TestTransactionScope
    {
        private readonly DbContext context;

        private IDbContextTransaction transaction;

        public TestTransactionScope(DbContext context)
        {
            this.context = context;
        }

        public void CreateTransaction()
        {

            var connection = context.GetService<IRelationalConnection>();
            if (connection == null) return;
            if (connection?.CurrentTransaction != null) return;

            transaction = connection.BeginTransaction();
        }

        public void RollBackTransaction()
        {
            if (transaction == null) return;

            transaction.Rollback();
        }
    }
}
