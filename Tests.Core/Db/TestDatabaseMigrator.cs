using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Tests.Core.Db
{
    public class TestDatabaseMigrator
    {
        private readonly DbContext context;

        public TestDatabaseMigrator(DbContext context)
        {
            this.context = context;
        }

        public void Create()
        {
            context.Database.Migrate();//Create DB
        }

        public void RunMigrations(Assembly resourceAssembly)
        {
            var toRun = GetPendingMigrations(resourceAssembly);

            var executor = new SqlMigrationExecutor();
            executor.Execute(context, resourceAssembly, toRun);
        }

        public IOrderedEnumerable<string> GetPendingMigrations(Assembly resourceAssembly)
        {
            var appliedMigrations = GetAppliedMigrations();
            var assemblyMigrations = resourceAssembly.GetManifestResourceNames();
            return assemblyMigrations.Where(r => r.EndsWith(".sql") && !appliedMigrations.Contains(GetMigrationId(r)))
                .OrderBy(r => r);
        }

        public void RunMigrations<T>(IEnumerable<ITestMigration<T>> testMigrations)
            where T: DbContext
        {
            var appliedMigrations = GetAppliedMigrations();
            var toRun = testMigrations.Where(m => !appliedMigrations.Contains(m.Name)).ToList();

            var executor = new TestMigrationExecutor();
            executor.Execute(context, toRun);
        }

        private static string GetMigrationId(string resourceName)
        {
            return resourceName.Replace(".sql", string.Empty).Split('.').Last();
        }

        private HashSet<string> GetAppliedMigrations()
        {
            const string sql = "IF OBJECT_ID(N'__EFMigrationsHistory') IS NOT NULL SELECT MigrationId FROM __EFMigrationsHistory";
            var result = context.Database.ExecuteSqlWithResult(sql, reader => reader[0].ToString());

            return new HashSet<string>(result);
        }
    }
}
