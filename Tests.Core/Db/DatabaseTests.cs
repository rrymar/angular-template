﻿using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using System.Reflection;

namespace Tests.Core.Db
{
    public static class DatabaseTests
    {
        public static void IsMigrationGenerated(this DbContext context)
        {
            var migrationsAssembly = context.GetService<IMigrationsAssembly>();
            var differ = context.GetService<IMigrationsModelDiffer>();

            var hasDifferences = differ.HasDifferences(migrationsAssembly.ModelSnapshot.Model, context.Model);

            hasDifferences.Should().BeFalse("All changes should generate migrations");

        }

        public static void IsMigrationFileNamedAsMigrationId(this DbContext context,
            Assembly assembly)
        {
            var dbManager = new TestDatabaseMigrator(context);
            var migrations = dbManager.GetPendingMigrations(assembly);
            migrations.Should().BeEmpty();
        }
    }
}
