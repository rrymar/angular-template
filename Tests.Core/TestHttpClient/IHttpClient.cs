using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Core.TestHttpClient
{
    public interface IHttpClient : IDisposable
    {
        event EventHandler OnBeforeRequest;

        void AddHeader(string name, string value);

        void RemoveHeader(string name);

        Task<HttpResponseMessage> GetAsync(string requestUri);

        Task<HttpResponseMessage> PostAsync(string requestUri, HttpContent content);

        Task<HttpResponseMessage> PutAsync(string requestUri, HttpContent content);

        Task<HttpResponseMessage> DeleteAsync(string requestUri);

        Task<HttpResponseMessage> SendAsync(HttpRequestMessage request);

    }
}
