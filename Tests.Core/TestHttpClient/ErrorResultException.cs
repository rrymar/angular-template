using System;
using System.Net;

namespace Tests.Core.TestHttpClient
{
    public class ErrorResultException : Exception
    {
        public HttpStatusCode StatusCode { get; set; }

        public ErrorResult ErrorResult { get; }

        public override string Message => $"Http status: {StatusCode}; {ErrorResult}";

        public ErrorResultException(ErrorResult errorResult, HttpStatusCode statusCode)
        {
            ErrorResult = errorResult;
            StatusCode = statusCode;
        }

        public ErrorResultException() : base()
        {
        }

        protected ErrorResultException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context)
        {
        }

        public ErrorResultException(string message) : base(message)
        {
        }

        public ErrorResultException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
