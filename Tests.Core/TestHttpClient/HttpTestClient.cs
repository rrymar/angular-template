using Autofac;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Core.TestHttpClient
{
    public class HttpTestClient : IHttpClient
    {
        public event EventHandler OnBeforeRequest;

        public HttpClient InnerClient { get; }

        public ILifetimeScope Scope { get; }

        public HttpTestClient(HttpClient innerClient, ILifetimeScope scope)
        {
            InnerClient = innerClient;
            Scope = scope;
        }

        public HttpRequestHeaders DefaultRequestHeaders => InnerClient.DefaultRequestHeaders;

        public Task<HttpResponseMessage> GetAsync(string requestUri)
        {
            return DoAction(() => InnerClient.GetAsync(requestUri));
        }

        public Task<HttpResponseMessage> PostAsync(string requestUri, HttpContent content)
        {
            return DoAction(() => InnerClient.PostAsync(requestUri, content));
        }

        public Task<HttpResponseMessage> PutAsync(string requestUri, HttpContent content)
        {
            return DoAction(() => InnerClient.PutAsync(requestUri, content));
        }

        public Task<HttpResponseMessage> DeleteAsync(string requestUri)
        {
            return DoAction(() => InnerClient.DeleteAsync(requestUri));
        }

        public Task<HttpResponseMessage> SendAsync(HttpRequestMessage request)
        {
            return DoAction(() => InnerClient.SendAsync(request));
        }

        private T DoAction<T>(Func<T> inner)
        {
            if (OnBeforeRequest != null)
                OnBeforeRequest.Invoke(this,EventArgs.Empty);
            return inner();
        }

        public void Dispose()
        {
            InnerClient?.Dispose();
        }

        public void AddHeader(string name, string value)
        {
            InnerClient.DefaultRequestHeaders.Add(name, value);
        }

        public void RemoveHeader(string name)
        {
            InnerClient.DefaultRequestHeaders.Remove(name);
        }
    }
}
