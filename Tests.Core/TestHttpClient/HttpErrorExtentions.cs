using FluentAssertions;
using FluentAssertions.Specialized;
using System;
using System.Linq;
using System.Net;

namespace Tests.Core.TestHttpClient
{
    public static class HttpErrorExtentions
    {
        public static ExceptionAssertions<ErrorResultException> ShouldThrowValidationError<T>(this Func<T> action, params string[] messages)
        {
            return action.ShouldThrowHttpError((HttpStatusCode)422, messages);
        }

        public static ExceptionAssertions<ErrorResultException> ShouldThrowHttpError<T>(this Func<T> action, string message)
        {
            var assertions = action.Should().Throw<ErrorResultException>();
            var exception = assertions.And;
            VerifyMessages(exception, message);
            return assertions;
        }

        public static ExceptionAssertions<ErrorResultException> ShouldThrowHttpError<T>(this Func<T> action, HttpStatusCode code, params string[] messages)
        {
            var assertions = action.Should().Throw<ErrorResultException>();
            var exception = assertions.And;
            VerifyCode(exception, code);
            VerifyMessages(exception, messages);
            return assertions;
        }

        private static void VerifyMessages(ErrorResultException exception, params string[] messages)
        {
            if (messages.Any())
            {
                var errors = exception.ErrorResult?.Errors ?? new string[] { };
                messages.All(m => errors.Any(e => e.Contains(m))).Should().BeTrue();
            }
        }

        private static void VerifyCode(ErrorResultException exception, HttpStatusCode code)
        {
            exception.StatusCode.Should().Be(code);
        }
    }
}
