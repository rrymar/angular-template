using System.Net.Http;
using System.Net.Http.Headers;

namespace Tests.Core.TestHttpClient
{
    public static class HttpClientExtensions
    {
        public static HttpClient AcceptJson(this HttpClient client)
        {
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            return client;
        }

        public static void AddJwtToken(this IHttpClient client, string token)
        {
            client.RemoveHeader("Authorization");
            client.AddHeader("Authorization", $"Bearer {token}");
        }
    }
}
