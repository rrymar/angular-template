using Autofac;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.Core.TestApp
{
    public class FixtureScope : IServiceScope
    {
        private readonly ILifetimeScope lifetimeScope;

        private readonly bool sharedScope;

        public FixtureScope(ILifetimeScope lifetimeScope, bool sharedScope)
        {
            this.sharedScope = sharedScope;
            this.lifetimeScope = lifetimeScope;
            ServiceProvider = this.lifetimeScope.Resolve<IServiceProvider>();

        }

        public IServiceProvider ServiceProvider { get; }

        public void Dispose()
        {
            if (!sharedScope)
            {
                lifetimeScope.Dispose();
            }
        }
    }
}
