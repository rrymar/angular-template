using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.TestHost;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Hosting;
using System.Reflection;
using System.Net.Http;
using Microsoft.AspNetCore.Http;
using System.Threading;
using Autofac;
using Tests.Core.TestHttpClient;

namespace Tests.Core.TestApp
{
    public class TestApplicationServer : TestServer
    {
        public IHttpApplication<HostingApplication.Context> AppInstance { get; }

        public TestApplicationServer(IWebHostBuilder builder) : base(builder)
        {
            var appInstanceField = GetType().GetTypeInfo().BaseType.GetTypeInfo().GetDeclaredField("_application");
            AppInstance = (IHttpApplication<HostingApplication.Context>)appInstanceField.GetValue(this);
        }

        private HttpMessageHandler CreateScopedHandler(ILifetimeScope scope)
        {
            return new ClientHandler(
                BaseAddress == null ? PathString.Empty : PathString.FromUriComponent(BaseAddress),
                new HttpApplicationProxy(AppInstance, scope)
            );
        }

        public IHttpClient CreateScopedClient(ILifetimeScope scope)
        {
            return new HttpTestClient(new HttpClient(CreateScopedHandler(scope))
            {
                BaseAddress = BaseAddress,
                Timeout = Timeout.InfiniteTimeSpan
            }.AcceptJson(), scope);
        }
    }
}
