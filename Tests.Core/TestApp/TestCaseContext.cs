using Autofac;
using System;
using Tests.Core.TestHttpClient;

namespace Tests.Core.TestApp
{
    public class TestCaseContext : IDisposable
    {
        public ILifetimeScope Scope { get; }

        public IHttpClient Client { get; }

        public TestCaseContext(ILifetimeScope scope, IHttpClient client)
        {
            Scope = scope;
            Client = client;
        }

        public void Dispose()
        {
            Client.Dispose();
            Scope.Dispose();
        }
    }
}
