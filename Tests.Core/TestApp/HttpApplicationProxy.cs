using Autofac;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Http.Features;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Core.TestApp
{
    public class HttpApplicationProxy : IHttpApplication<HostingApplication.Context>
    {
        private readonly IHttpApplication<HostingApplication.Context> next;

        private readonly ILifetimeScope scope;

        public HttpApplicationProxy(IHttpApplication<HostingApplication.Context> next, ILifetimeScope scope)
        {
            this.next = next;
            this.scope = scope;
        }

        public HostingApplication.Context CreateContext(IFeatureCollection contextFeatures)
        {
            return next.CreateContext(contextFeatures);
        }

        public Task ProcessRequestAsync(HostingApplication.Context context)
        {
            FixtureScopeFactory.FixtureScope = scope;
            return next.ProcessRequestAsync(context);
        }

        public void DisposeContext(HostingApplication.Context context, Exception exception)
        {
            next.DisposeContext(context, exception);
        }
    }
}
