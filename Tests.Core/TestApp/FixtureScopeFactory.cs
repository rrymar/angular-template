using Autofac;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tests.Core.TestApp
{
    public class FixtureScopeFactory : IServiceScopeFactory
    {
        private readonly ILifetimeScope _lifetimeScope;

        [ThreadStatic]
        public static ILifetimeScope FixtureScope;

        public FixtureScopeFactory(ILifetimeScope lifetimeScope)
        {
            _lifetimeScope = lifetimeScope;
        }

        public IServiceScope CreateScope()
        {
            if (FixtureScope == null || FixtureScope == _lifetimeScope)
                return new FixtureScope(_lifetimeScope.BeginLifetimeScope(), false);
            return new FixtureScope(FixtureScope, true);
        }
    }
}
