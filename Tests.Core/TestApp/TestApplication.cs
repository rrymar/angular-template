using Autofac;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Tests.Core.TestApp
{
    public class TestApplication<TStartup> : IDisposable
            where TStartup : class
    {
        private TestApplicationServer server;

        public ILifetimeScope RootScope { get; protected set; }

        public TestApplication()
        {
            var builder = new WebHostBuilder()
                .UseStartup<TStartup>()
                .ConfigureServices(ConfigureScopeFactoryService);
            server = new TestApplicationServer(builder);
            RootScope = server.Host.Services.GetService<ILifetimeScope>();
        }

        private static void ConfigureScopeFactoryService(IServiceCollection collection)
        {
            collection.Add(new ServiceDescriptor(
                    typeof(IServiceScopeFactory), typeof(FixtureScopeFactory),
                ServiceLifetime.Scoped));
        }

        public TestCaseContext InitTestCase()
        {
            var scope = RootScope.BeginLifetimeScope();
            var client = server.CreateScopedClient(scope);

            return new TestCaseContext(scope, client);
        }

        public void Dispose()
        {
            if (server == null)
                return;
            server.Dispose();
            server = null;
        }
    }
}
