﻿using Autofac;
using Microsoft.EntityFrameworkCore;
using System;
using Tests.Core.Db;
using Tests.Core.TestApp;
using Tests.Core.TestClient;

namespace Tests.Core
{
    public abstract class IntegrationTest<T> : IDisposable
        where T : class
    {
        protected readonly TestApplication<T> fixture;

        private readonly TestCaseContext testCaseContext;

        private readonly TestTransactionScope transactionScope;

        public ILifetimeScope Scope => testCaseContext.Scope;

        protected RestClient TestClient { get; }

        protected abstract DbContext Database { get; }

        protected virtual bool CreateTransaction => true;

        protected IntegrationTest(TestApplication<T> fixture)
        {
            this.fixture = fixture;

            testCaseContext = this.fixture.InitTestCase();
            testCaseContext.Client.OnBeforeRequest += HttpClient_OnBeforeRequest;

            TestClient = new RestClient(testCaseContext.Client);

            if (CreateTransaction)
            {
                transactionScope = new TestTransactionScope(Database);
                transactionScope.CreateTransaction();
            }
        }

        private void HttpClient_OnBeforeRequest(object sender, EventArgs e)
        {
            Database?.ClearCache();
        }

        public virtual void Dispose()
        {
            transactionScope?.RollBackTransaction();
            testCaseContext?.Dispose();
        }
    }
}
