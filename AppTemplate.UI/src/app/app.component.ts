import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticationStore } from './authentication/authentication.store';
import { Authentication } from './authentication/authentication.reducer';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AppTemplate';

  isLoggedIn$: Observable<boolean>;

  constructor(private authStore: AuthenticationStore) {
    this.authStore.dispatch(new Authentication.GetCurrentUserAction(false));
    this.isLoggedIn$ = authStore.isLoggedIn$;
  }
}
