import {
  MatButtonModule,
  MatCardModule,
  MatInputModule,
  MatFormFieldModule,
  MatGridListModule,
  MatToolbarModule,
  MatTooltipModule,
  MatSidenavModule,
  MatDividerModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatProgressSpinnerModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatAutocompleteModule,
  MatListModule,
  MatIconModule,
  MatSelectModule,
  MatTabsModule,
  MatRadioModule,
  MatExpansionModule,
  MatMenuModule,
  MatProgressBarModule,
  MatDialogModule,
  MatNativeDateModule,
} from '@angular/material';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';

import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DecimalPipe } from '@angular/common';
import { EditableCanDeactivateGuard } from './core/change-tracking/editable-can-deactivate.guard';

@NgModule({
  imports: [
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatGridListModule,
    MatListModule,
    MatIconModule,
    MatToolbarModule,
    MatTooltipModule,
    MatSidenavModule,
    MatDividerModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatTabsModule,
    MatRadioModule,
    MatExpansionModule,
    MatMenuModule,
    MatProgressBarModule,
    MatDialogModule,
    NgProgressModule,
    NgProgressHttpModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    CommonModule,
    RouterModule,
    MatProgressSpinnerModule,
  ],
  providers: [
    DecimalPipe,
    EditableCanDeactivateGuard
  ],
  declarations: [
  ],
  exports: [
    MatButtonModule,
    MatCardModule,
    MatInputModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatGridListModule,
    MatToolbarModule,
    MatTooltipModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    MatDividerModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatTabsModule,
    MatRadioModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatMenuModule,
    NgProgressModule,
    NgProgressHttpModule,
    FormsModule,
    MatExpansionModule,
    MatProgressBarModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    CommonModule,
    MatDialogModule,
    MatProgressSpinnerModule,
  ]
})
export class UiModule { }
