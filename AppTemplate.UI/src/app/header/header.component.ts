import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticationStore } from '../authentication/authentication.store';
import { Authentication } from '../authentication/authentication.reducer';
import { AuthenticationRoutes } from '../authentication/authentication-routes';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isLoggedIn$: Observable<boolean>;
  loginLink = AuthenticationRoutes.login;

  constructor(private authStore: AuthenticationStore) {
    this.isLoggedIn$ = authStore.isLoggedIn$;
  }

  ngOnInit() {
  }

  onLogout() {
    this.authStore.dispatch(new Authentication.LogoutAction());
  }
}
