import { OnDestroy } from "@angular/core";
import { MonoTypeOperatorFunction } from "rxjs";
import { takeWhile } from "rxjs/operators";

export abstract class DestroyableComponent implements OnDestroy {

  protected isDestroyed = false;
  ngOnDestroy() {
    this.isDestroyed = true;
  }

  untillDestroyed<T>(): MonoTypeOperatorFunction<T> {
    return takeWhile(() => !this.isDestroyed)
  }
}
