import { HostListener } from "@angular/core";
import { DestroyableComponent } from "../destroyable-component";
import { Deactivable } from "./deactivable";
import { RouterStateSnapshot } from "@angular/router";

export abstract class EditableComponent extends DestroyableComponent implements Deactivable {

  abstract canDeactivate(currentState?: RouterStateSnapshot, nextState?: RouterStateSnapshot): boolean;

  @HostListener('window:beforeunload', ['$event'])
  unloadNotification($event: any) {
    if (!this.canDeactivate()) {
      $event.returnValue = true;
    }
  }
}

