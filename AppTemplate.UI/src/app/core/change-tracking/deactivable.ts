import { RouterStateSnapshot } from "@angular/router";

export interface Deactivable  {
  canDeactivate(currentState?: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): boolean;
}
