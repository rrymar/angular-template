import { Injectable } from '@angular/core';
import { CanDeactivate, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Deactivable } from './deactivable';

export const LoseChangesMessage = 'Do you want to leave this page?\nChanges you made may not be saved.';

@Injectable()
export class EditableCanDeactivateGuard implements CanDeactivate<Deactivable> {
  canDeactivate(component: Deactivable, currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot, nextState?: RouterStateSnapshot): boolean {

    if (!component.canDeactivate(currentState, nextState)) {
      if (confirm(LoseChangesMessage)) {
        return true;
      } else {
        return false;
      }
    }
    return true;
  }
}
