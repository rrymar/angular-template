import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { ToastrService } from "ngx-toastr";
import { SearchQuery } from "./search-query";
import { CrudService } from "./crud.service";

export abstract class NotifiableCrudService<T, TSearchQuery extends SearchQuery>
  extends CrudService<T, TSearchQuery> {

  constructor(httpClient: HttpClient,
    private toastr: ToastrService) {
    super(httpClient);
  }

  public abstract getEntityTypeName(): string;

  public abstract getEntityName(entity: T): string;

  protected getUpdateOperation(e: T) : string {
    return "updated";
  }

  create(entity: T): Observable<T> {
    return super.create(entity).pipe(map(e => {
      this.toastr.success(this.getMessage(e, 'created'));
      return e;
    }));
  }

  update(id: string, entity: T): Observable<T> {
    return this.httpClient.put<T>(this.getUrl(id), entity).pipe(map(e => {
      this.toastr.success(this.getMessage(e, this.getUpdateOperation(e)));
      return e;
    }));
  }

  private getMessage(entity: T, operation: string): string {
    return `${this.getEntityTypeName()} ${this.getEntityName(entity)} was successfully ${operation}.`;
  }
}
