import { NgModule } from "@angular/core";
import { ShowPasswordForDirective } from "./show-password-for.directive";
import { ShowPasswordService } from "./show-password.service";
import { RegisterPasswordInputAsDirective } from "./register-password-input-as.directive";

@NgModule({
  exports: [ShowPasswordForDirective, RegisterPasswordInputAsDirective],
  declarations: [ShowPasswordForDirective, RegisterPasswordInputAsDirective],
  providers: [ShowPasswordService]
})
export class ShowPasswordModule {
}
