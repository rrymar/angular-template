import { Directive, OnInit, OnDestroy, ElementRef, Renderer2 } from "@angular/core";
import { Subscription } from "rxjs";
import { ShowPasswordService } from "./show-password.service";

@Directive({
  selector: 'input[registerPasswordInputAs]'
})
export class RegisterPasswordInputAsDirective implements OnInit, OnDestroy {
  private name: string;
  private subscription: Subscription;

  constructor(private service: ShowPasswordService,
    private el: ElementRef,
    private renderer: Renderer2
  ) {
    this.name = this.el.nativeElement.getAttribute('name');
    if (!this.name)
      throw new Error('Input should have name attribute.');
    
    this.service.setShow(this.name, this.el.nativeElement.type !== 'password');
  }

  ngOnInit(): void {
    this.subscription = this.service
      .getObservable(this.name)
      .subscribe(show => {
        this.renderer.setAttribute(this.el.nativeElement, 'type', show ? 'text' : 'password');
      });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
