import { Directive, Input, HostListener, ElementRef, Renderer2, OnInit, OnDestroy } from "@angular/core";
import { ShowPasswordService } from "./show-password.service";
import { Subscription } from "rxjs";

@Directive({
  selector: '[showPasswordFor]'
})
export class ShowPasswordForDirective implements OnInit, OnDestroy {
  private subscription: Subscription;

  @Input() showPasswordFor?: string;

  constructor(private service: ShowPasswordService,
    private el: ElementRef,
    private renderer: Renderer2) {

  }

  ngOnInit(): void {
    if (!this.showPasswordFor)
      throw new Error('Please specify input name.');

    this.subscription = this.service.getObservable(this.showPasswordFor)
      .subscribe(show => {
        const icon = show ? 'visibility_off' : 'visibility';
        this.renderer.setProperty(this.el.nativeElement, 'innerHTML', icon);
      });
  }

  @HostListener('click')
  onClick() {
    this.service.toggleShow(this.showPasswordFor);
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
