import { Injectable } from '@angular/core';
import { Observable, Subject, ReplaySubject } from 'rxjs';

interface IState {
  key: string;
  show: boolean;
  subject?: Subject<boolean>;
}

@Injectable()
export class ShowPasswordService {
  private readonly states = [];

  constructor() { }

  private getState(key: string): IState {
    let state = this.states.find(s => s.key == key);
    if (!state && key) {
      state = this.init(key);
    }
    return state;
  }

  private init(key: string): IState {
    const subject = new ReplaySubject<boolean>(1);
    const state = {
      key,
      show: false,
      subject
    };
    this.states.push(state);
    return state;
  }

  private save(state: IState, show: boolean) {
    state.show = show;
    state.subject.next(state.show);
  }

  public getObservable(id: string): Observable<boolean> {
    return this.getState(id).subject;
  }

  public setShow(key: string, show: boolean): void {
    this.save(this.getState(key), show);
  }

  public toggleShow(key: string): void {
    const state = this.getState(key);
    this.save(state, !state.show);
  }
}
