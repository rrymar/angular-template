import { Action } from '@ngrx/store';

export abstract class StateAction<TState> implements Action {
  type: string;
  abstract reduce(state: TState): TState;
}

