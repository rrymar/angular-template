import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ResultsList } from './results-list';
import { SearchQuery } from './search-query';

export abstract class CrudService<T, TSearchQuery extends SearchQuery> {
  constructor(protected httpClient: HttpClient) { }

  public abstract getBasePath(): string;

  protected getUrl(path: string = ''): string {
    const url = this.getBasePath();
    if (path === '') {
      return url;
    }

    return url + '/' + path;

  };

  get(id: string = ''): Observable<T> {
    return this.httpClient.get<T>(this.getUrl(id));
  }

  load(query: TSearchQuery): Observable<ResultsList<T>> {
    const httpQuery = this.createQuery(query);

    return this.httpClient.get<ResultsList<T>>(this.getUrl(),
      { params: httpQuery });
  }

  protected createQuery(query: TSearchQuery) {
    let httpQuery = new HttpParams();
    if (query) {
      for (const key in query) {
        let value = query[key];
        if (value || (typeof value === "boolean" && value === false)) {
          httpQuery = httpQuery.set(key.toString(), value.toString());
        }
      }
    }
    return httpQuery;
  }

  create(entity: T): Observable<T> {
    return this.httpClient.post<T>(this.getUrl(), entity);
  }

  update(id: string, entity: T): Observable<T> {
    return this.httpClient.put<T>(this.getUrl(id), entity);
  }

  delete(id: string): Observable<object> {
    return this.httpClient.delete(this.getUrl(id));
  }
}
