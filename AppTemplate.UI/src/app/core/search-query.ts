import { MatPaginator, MatSort } from "@angular/material";

export class SearchQuery {
  keyword: string = '';

  pageIndex: number = 0;

  pageSize: number = 10;

  sortField: string;

  isDesc: boolean = true;

  isActive: boolean = true;
}

export function applyToGrid(query: SearchQuery, paginator: MatPaginator, sort: MatSort) {
  if (paginator) {
    paginator.pageIndex = query.pageIndex;
    paginator.pageSize = query.pageSize;
  }

  if (sort) {
    sort.active = query.sortField;
    sort.direction = query.isDesc ? 'desc' : 'asc';
  }
}

export function loadFromGrid(query: SearchQuery, paginator: MatPaginator, sort: MatSort): SearchQuery {
  var toReturn = { ...query };
  if (paginator) {
    toReturn.pageIndex = paginator.pageIndex;
    toReturn.pageSize = paginator.pageSize;
  }

  if (sort) {
    toReturn.sortField = sort.active;
    toReturn.isDesc = sort.direction == 'desc';
  }

  return toReturn;
}
