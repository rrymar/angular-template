import { Actions, Effect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { ErrorAction } from './error.action';
import { ErrorsService } from './errors.service';

@Injectable()
export class ErrorEffects {
  constructor(private actions$: Actions,
    private errorsService: ErrorsService) {
  }

  @Effect({ dispatch: false })
  loadUser$ = this.actions$.pipe(
    ofType(ErrorAction.actionType),
    tap((a: ErrorAction) => this.errorsService.handleError(a.error))
  );

}
