import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { ServerError } from './server-error.model';
import { UiError } from './ui-error';
import { ToastrService } from 'ngx-toastr';

@Injectable({ providedIn: 'root' })
export class ErrorsService {
  constructor(private toastr: ToastrService) { }

  handleError(error: Error) {
    if (error instanceof HttpErrorResponse) {
      if (error.status === 401) {
        return;
      }

      if (error.status === 403) {
        this.toastr.error('Access Forbidden');
        return;
      }

      if (error.status === 404) {
        this.toastr.error('Not Found');
        return;
      }

      let serverError = error.error as ServerError;
      console.error(serverError);

      if (error.status >= 500) {
        const message = `Error Code: ${error.status}; Oops! Something went wrong.`;
        this.toastr.error(message);
        return;
      }

      for (let index in serverError.errors) {
        let message = serverError.errors[index];
        if (message === '')
          message = error.statusText;
        if (error.status == 422)
          this.toastr.warning(message.slice(0, 200));
        else
          this.toastr.error(message.slice(0, 200), serverError.requestId);

      }
      return;
    }

    if (error instanceof UiError) {
      this.toastr.error(error.message);
    }
    else {
      console.error(error);
    }
  }
}

