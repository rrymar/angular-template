import { Action } from "@ngrx/store";

export class ErrorAction implements Action {
  static readonly actionType = 'Error';
  readonly type = ErrorAction.actionType;

  constructor(public error: Error) {}
}
