import { Actions, Effect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { UsersService } from '../users.service';
import { map, switchMap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { of, Observable } from 'rxjs';
import { Action } from '@ngrx/store';
import { ErrorAction } from '../../errors/error.action';
import { UsersDetail } from './user-detail.reducer';
import { UsersRoutes } from '../users-routes';

@Injectable()
export class UserDetailEffects {
  constructor(private actions$: Actions,
    private service: UsersService,
    private router: Router) {
  }

  @Effect()
  loadUser$ = this.actions$.pipe(
    ofType(UsersDetail.StartLoadingAction.actionType),
    switchMap(a => this.onLoadUser(a)));

  private onLoadUser(a: UsersDetail.StartLoadingAction): Observable<Action> {
    return this.service.get(a.id).pipe(
      map(r => new UsersDetail.EndLoadingAction(r)),
      catchError(e => of(new UsersDetail.EndLoadingAction(null), new ErrorAction(e)))
    );
  }

  @Effect()
  saveUser$ = this.actions$.pipe(
    ofType(UsersDetail.SaveAction.actionType),
    switchMap(a => this.onSaveUser(a)));

  private onSaveUser(a: UsersDetail.SaveAction): Observable<Action> {
    const result = !!a.entity.id
      ? this.service.update(a.entity.id.toString(), a.entity)
      : this.service.create(a.entity);

    return result.pipe(
      map(() => new UsersDetail.GotoListAction()),
      catchError(e => of(new ErrorAction(e)))
    );
  }

  @Effect({ dispatch: false })
  gotoList$ = this.actions$.pipe(
    ofType(UsersDetail.GotoListAction.actionType),
    map(() => this.onGotoList())
  );

  private onGotoList() {
    this.router.navigate([UsersRoutes.users]);
  }
}
