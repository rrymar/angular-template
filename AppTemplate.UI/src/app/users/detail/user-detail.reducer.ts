import { UserModel } from "../user.model";
import { Action } from '@ngrx/store';
import { StateAction } from "../../core/ngrx/state-action";

export namespace UsersDetail {
  export interface State {
    entity: UserModel;
    id: string;
    isLoading: boolean;
  }

  export const InitialState: State = {
    entity: new UserModel(),
    id: null,
    isLoading: false
  }

  export class ResetAction implements StateAction<State> {
    static readonly actionType = '[User] Reset'
    readonly type = ResetAction.actionType;

    constructor() { }

    reduce(state: State): State {
      return InitialState;
    }
  }

  export class StartLoadingAction implements StateAction<State> {
    static readonly actionType = '[User] Start Loading'
    readonly type = StartLoadingAction.actionType;

    constructor(public id: string) { }

    reduce(state: State): State {
      return {
        ...state,
        id: this.id,
        isLoading: true
      }
    }
  }

  export class EndLoadingAction implements StateAction<State> {
    static readonly actionType = '[User] End Loading'
    readonly type = EndLoadingAction.actionType;

    constructor(private entity: UserModel) { }

    reduce(state: State): State {
      return {
        ...state,
        entity: this.entity,
        isLoading: false
      }
    }
  }


  export class SaveAction implements StateAction<State> {
    static readonly actionType = '[User] Save'
    readonly type = SaveAction.actionType;

    constructor(public entity: UserModel) { }

    reduce(state: State): State {
      return {
        ...state,
        entity: this.entity
      }
    }
  }

  export class GotoListAction implements StateAction<State> {
    static readonly actionType = '[User] Go to List'
    readonly type = GotoListAction.actionType;

    constructor() { }

    reduce(state: State): State {
      return state;
    }
  }

  const supportedActions = [
    EndLoadingAction.actionType, StartLoadingAction.actionType,
    SaveAction.actionType, GotoListAction.actionType,
    ResetAction.actionType
  ];

  export function reducer(state = InitialState, action: Action): State {
    if (!supportedActions.includes(action.type)) return state;
    return (action as StateAction<State>).reduce(state);
  }
}
