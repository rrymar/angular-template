import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, RouterStateSnapshot } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';

import { UserDetailStore } from './user-detail.store';
import { UsersDetail } from './user-detail.reducer';
import { EditableComponent } from '../../core/change-tracking/editable-component';
import { debounceTime, tap } from 'rxjs/operators';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent extends EditableComponent implements OnInit {
  canSave = false;

  isLoading$ = this.store.isLoading$.pipe(debounceTime(500));

  userForm = this.fb.group({
    id: [0],
    username: ['', [Validators.required, Validators.maxLength(32)]],
    isActive: [''],
    password: ['', Validators.maxLength(32)],
    firstName: ['', Validators.maxLength(50)],
    lastName: ['', Validators.maxLength(50)],
    email: ['', [Validators.required, Validators.maxLength(200)]],
  });

  constructor(
    private fb: FormBuilder,
    private cdRef: ChangeDetectorRef,
    private store: UserDetailStore,
    private route: ActivatedRoute) {

    super();

    this.store.entity$.pipe(this.untillDestroyed()).subscribe(v => {
      cdRef.markForCheck();
      this.userForm.patchValue(v);
    });
  }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.canSave = true;

    if (!id) return;

    this.userForm.get('username').disable();

    this.store.dispatch(new UsersDetail.StartLoadingAction(id));
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.store.dispatch(new UsersDetail.ResetAction());
  }

  onSubmit() {
    if (!this.userForm.valid) {
      return;
    }

    this.store.dispatch(new UsersDetail.SaveAction(this.userForm.getRawValue()));
    this.userForm.markAsPristine();
  }

  canDeactivate(currentState?: RouterStateSnapshot, nextState?: RouterStateSnapshot): boolean {
    return !this.userForm.dirty;
  }
}
