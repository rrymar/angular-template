import { UserModel } from "../user.model";
import { Action, createSelector, Store } from '@ngrx/store';
import { Injectable } from "@angular/core";
import { getUsersModuleState, UsersState } from "../users.reducer";
import { Observable } from "rxjs";

@Injectable()
export class UserDetailStore {
  constructor(private store$: Store<UsersState>) {
  }

  private getDetailState = createSelector(getUsersModuleState, s => s.detail);

  private getEntity = createSelector(this.getDetailState, s => s.entity);

  get entity$(): Observable<UserModel> {
    return this.store$.select(this.getEntity);
  }

  private getIsLoading = createSelector(this.getDetailState, s => s.isLoading);

  get isLoading$(): Observable<boolean> {
    return this.store$.select(this.getIsLoading);
  }


  dispatch(action: Action) {
    this.store$.dispatch(action);
  }
}
