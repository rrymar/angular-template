import { Component, OnInit, ViewChild, AfterViewInit, ChangeDetectionStrategy, AfterViewChecked, AfterContentInit } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { merge, Observable } from 'rxjs';

import { UserModel } from '../user.model';
import { Router } from '@angular/router';

import { UsersList } from './users-list.reducer';
import { UsersListStore } from './users-list.store';
import { SearchQuery, loadFromGrid } from '../../core/search-query';
import { DestroyableComponent } from '../../core/destroyable-component';
import { debounceTime, map } from 'rxjs/operators';
import { UsersRoutes } from '../users-routes';

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-users',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent extends DestroyableComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['id', 'username', 'fullName', 'email', 'createdOn', 'updatedOn'];

  items$ = this.store.items$;

  isLoading$ = this.store.isLoading$.pipe(debounceTime(500));

  sortField$ = this.store.searchQuery$.pipe(map(r => r.sortField));
  sortDirection$ = this.store.searchQuery$.pipe(map(r => r.isDesc ? 'desc' : 'asc'));

  pageSize$ = this.store.searchQuery$.pipe(map(r => r.pageSize));
  pageIndex$ = this.store.searchQuery$.pipe(map(r => r.pageIndex));

  totalCount$ = this.store.totalCount$;

  query = new SearchQuery();

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private router: Router,
    private store: UsersListStore,
  ) {
    super();

    this.store.searchQuery$.pipe(this.untillDestroyed()).subscribe(v => {
      this.query = v;
    });
  }

  ngOnInit() {
    this.loadGrid();
  }

  ngAfterViewInit() {
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
    merge(this.sort.sortChange, this.paginator.page).subscribe(() => {
      this.query = loadFromGrid(this.query, this.paginator, this.sort);
      this.loadGrid();
    });
  }

  reloadGrid() {
    this.paginator.pageIndex = 0;
    this.loadGrid();
  }

  loadGrid() {
    this.store.dispatch(new UsersList.StartLoadingAction(this.query));
  }

  onRowAction(row: UserModel) {
    this.router.navigate([UsersRoutes.user(row.id)]);
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.store.dispatch(new UsersList.ResetAction());
  }
}
