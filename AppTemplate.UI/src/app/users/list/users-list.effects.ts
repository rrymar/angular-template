import { Actions, Effect, ofType } from "@ngrx/effects";
import { Injectable } from "@angular/core";
import { UsersService } from "../users.service";
import { map, switchMap, catchError } from "rxjs/operators";
import { UsersList } from "./users-list.reducer";
import { of } from "rxjs";
import { ErrorAction } from "../../errors/error.action";

@Injectable()
export class UsersListEffects {
  constructor(private actions$: Actions,
    private service: UsersService) {
  }

  @Effect()
  searchUsers$ = this.actions$.pipe(
    ofType(UsersList.StartLoadingAction.actionType),
    switchMap(a => this.onSearchUsers(a)));

  private onSearchUsers(a: UsersList.StartLoadingAction) {
    return this.service.load(a.query).pipe(
      map(r => new UsersList.EndLoadingAction(r)),
      catchError(e => of(new UsersList.EndLoadingAction(null), new ErrorAction(e)))
    );
  }
}
