import { UserModel } from "../user.model";
import { Action, createSelector, Store } from '@ngrx/store';
import { Injectable } from "@angular/core";
import { getUsersModuleState, UsersState } from "../users.reducer";
import { Observable } from "rxjs";
import { SearchQuery } from "../../core/search-query";

@Injectable()
export class UsersListStore {
  constructor(private store$: Store<UsersState>) {
  }

  private getUsersState = createSelector(getUsersModuleState, s => s.list);
  private getUsersSelector = createSelector(this.getUsersState, s => s.users);


  private getItems = createSelector(this.getUsersSelector, s => s.items);

  get items$(): Observable<UserModel[]> {
    return this.store$.select(this.getItems);
  }

  private getTotalCount = createSelector(this.getUsersSelector, s => s.totalCount);

  get totalCount$(): Observable<number> {
    return this.store$.select(this.getTotalCount);
  }

  private getSearchQuery = createSelector(this.getUsersState, s => s.searchQuery);

  get searchQuery$(): Observable<SearchQuery> {
    return this.store$.select(this.getSearchQuery);
  }

  private getIsLoading = createSelector(this.getUsersState, s => s.isLoading);

  get isLoading$(): Observable<boolean> {
    return this.store$.select(this.getIsLoading);
  }

  dispatch(action: Action) {
    this.store$.dispatch(action);
  }
}
