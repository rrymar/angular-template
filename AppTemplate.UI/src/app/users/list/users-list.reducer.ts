import { UserModel } from '../user.model';
import { Action } from '@ngrx/store';
import { SearchQuery } from '../../core/search-query';
import { ResultsList } from '../../core/results-list';
import { StateAction } from '../../core/ngrx/state-action';

export namespace UsersList {
  export const defaultSearchQuery = new SearchQuery();

  export interface State {
    users: ResultsList<UserModel>;
    searchQuery: SearchQuery;
    isLoading: boolean;
  }

  export const InitialState: State = {
    users: new ResultsList<UserModel>(),
    searchQuery: defaultSearchQuery,
    isLoading: false
  }

  export class ResetAction implements StateAction<State> {
    static readonly actionType = '[Users] Reset'
    readonly type = ResetAction.actionType;

    constructor() { }

    reduce(state: State): State {
      return {
        ...InitialState,
        searchQuery: state.searchQuery
      }
    }
  }

  export class StartLoadingAction implements StateAction<State> {
    static readonly actionType = '[Users] Start Loading'
    readonly type = StartLoadingAction.actionType;

    constructor(public query: SearchQuery) { }

    reduce(state: State): State {
      return {
        ...state,
        searchQuery: { ...this.query },
        isLoading: true,
      }
    }
  }

  export class EndLoadingAction implements StateAction<State> {
    static readonly actionType = '[Users] End Loading'
    readonly type = EndLoadingAction.actionType;

    constructor(private users: ResultsList<UserModel>) { }

    reduce(state: State): State {
      return {
        ...state,
        users: this.users,
        isLoading: false
      };
    }
  }

  const supportedActions = [
    EndLoadingAction.actionType, StartLoadingAction.actionType,
    ResetAction.actionType
  ];

  export function reducer(state = InitialState, action: Action): State {
    if (!supportedActions.includes(action.type)) return state;
    return (action as StateAction<State>).reduce(state);
  }
}
