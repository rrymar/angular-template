
export class UserModel {
  id: number;
  login: string;
  password: string;
  firstName: string;
  lastName: string;
  fullName: string;
  email: string;

  createdOn: Date;
  updatedOn: Date;

  isActive: boolean = true;
}
