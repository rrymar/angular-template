import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { featureStore, usersReducers } from './users.reducer';
import { UsersListStore } from './list/users-list.store';
import { UserDetailStore } from './detail/user-detail.store';
import { UserDetailEffects } from './detail/user-detail.effects';
import { UsersListEffects } from './list/users-list.effects';
import { UiModule } from '../ui.module';
import { UsersService } from './users.service';
import { UserDetailComponent } from './detail/user-detail.component';
import { UsersListComponent } from './list/users-list.component';
import { EditableCanDeactivateGuard } from '../core/change-tracking/editable-can-deactivate.guard';
import { usersPath } from './users-routes';
import { ShowPasswordModule } from '../core/controls/show-password/show-password.module';

const routes: Routes =
  [{
    path: usersPath,
    children: [
      { path: '', component: UsersListComponent },
      {
        path: 'new',
        component: UserDetailComponent,
        canDeactivate: [EditableCanDeactivateGuard],
      },
      {
        path: ':id',
        component: UserDetailComponent,
        canDeactivate: [EditableCanDeactivateGuard],
      },
    ]
  }];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    EffectsModule.forFeature([UsersListEffects, UserDetailEffects]),
    StoreModule.forFeature(featureStore, usersReducers),
    UiModule,
    ShowPasswordModule,
  ],
  declarations: [
    UsersListComponent,
    UserDetailComponent,
  ],
  exports: [
    UsersListComponent,
    UserDetailComponent,
    RouterModule,
    ShowPasswordModule,
  ],
  providers: [UsersService, UsersListStore, UserDetailStore]
})
export class UsersModule {

}
