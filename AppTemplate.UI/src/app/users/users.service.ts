import { Injectable } from '@angular/core';
import { UserModel } from './user.model';
import { HttpClient } from '@angular/common/http';
import { NotifiableCrudService } from '../core/notifiable-crud.service';
import { SearchQuery } from '../core/search-query';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class UsersService extends NotifiableCrudService<UserModel, SearchQuery>  {

  getEntityTypeName() {
    return 'User';
  }

  getEntityName(entity: UserModel): string {
    return entity.fullName;
  }

  getBasePath(): string {
    return 'api/Users';
  }

  constructor(httpClient: HttpClient,
    toastr: ToastrService) {
    super(httpClient, toastr);
  }
}
