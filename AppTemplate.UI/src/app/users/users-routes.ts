export const usersPath = 'users';

export class UsersRoutes {
  static users = '/' + usersPath;

  static user(id: number): string {
    return UsersRoutes.users + '/' + id;
  }
}
