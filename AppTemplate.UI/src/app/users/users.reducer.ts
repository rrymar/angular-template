import {
  ActionReducerMap, createFeatureSelector,
} from '@ngrx/store';

import { UsersDetail } from './detail/user-detail.reducer';
import { UsersList } from './list/users-list.reducer';


export const featureStore = 'usersModule';

export interface UsersState {
  list: UsersList.State;
  detail: UsersDetail.State;
}
export const usersReducers: ActionReducerMap<UsersState> = {
  list: UsersList.reducer,
  detail: UsersDetail.reducer
};

export const getUsersModuleState = createFeatureSelector<UsersState>(featureStore);
