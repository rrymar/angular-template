import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { UiModule } from '../ui.module';
import { LoginComponent } from './login/login.component';
import { AuthenticationEffects } from './authentication.effects';
import { AuthenticationStore, featureStore } from './authentication.store';
import { Authentication } from './authentication.reducer';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { publicBasePath, loginPath, forgotPasswordPath, resetPasswordPath } from './authentication-routes';
import { ShowPasswordModule } from '../core/controls/show-password/show-password.module';

const routes: Routes =
  [
    {
      path: publicBasePath,
      children: [
        { path: loginPath, component: LoginComponent },
        { path: forgotPasswordPath, component: ForgotPasswordComponent },
        {
          path: resetPasswordPath,
          children: [{ path: ":id", component: ResetPasswordComponent }]
        },
      ]
    }
  ];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    EffectsModule.forFeature([AuthenticationEffects]),
    StoreModule.forFeature(featureStore, Authentication.reducer),
    UiModule,
    ShowPasswordModule
  ],
  declarations: [
    LoginComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
  ],
  exports: [
    LoginComponent,
    RouterModule,
    ShowPasswordModule
  ],
  providers: [AuthenticationStore]
})
export class AuthenticationModule {

}
