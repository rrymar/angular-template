import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AuthenticationRoutes } from '../authentication-routes';


@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
    private http: HttpClient) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
    });
  }


  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    this.http.post("api/Authentication/ResetPasswordRequest", this.form.value)
      .subscribe(() => {
        this.toastr.success("Email with instructions was sent.");
        this.router.navigate([AuthenticationRoutes.login]);
      });
  }
}
