import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationStore } from '../authentication.store';
import { Authentication } from '../authentication.reducer';
import { AuthenticationRoutes } from '../authentication-routes';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  forgotPasswordLink = AuthenticationRoutes.forgotPassword;

  constructor(
    private formBuilder: FormBuilder,
    private authStore: AuthenticationStore) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  isFieldInvalid(fieldName: string) {
    const field = this.form.get(fieldName);
    return field.valid && field.touched || field.untouched;
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    this.authStore.dispatch(new Authentication.LoginAction(this.form.value));
  }
}
