import { Action, createSelector, Store, createFeatureSelector } from '@ngrx/store';
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { CurrentUserModel } from './current-user.model';
import { Authentication } from './authentication.reducer';

export const featureStore = 'authenticationModule';

@Injectable()
export class AuthenticationStore {
  constructor(private store$: Store<Authentication.State>) {
  }

  private geModuleState = createFeatureSelector<Authentication.State>(featureStore);

  private getIsLoggedIn = createSelector(this.geModuleState, s => s.isLoggedIn);
  private getCurrentUser = createSelector(this.geModuleState, s => s.currentUser);


  get currentUser$(): Observable<CurrentUserModel> {
    return this.store$.select(this.getCurrentUser);
  }

  get isLoggedIn$(): Observable<boolean> {
    return this.store$.select(this.getIsLoggedIn);
  }

  dispatch(action: Action) {
    this.store$.dispatch(action);
  }
}
