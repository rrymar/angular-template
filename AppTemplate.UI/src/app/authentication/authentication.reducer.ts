import { Action } from '@ngrx/store';
import { StateAction } from '../core/ngrx/state-action';
import { CurrentUserModel } from './current-user.model';
import { LoginModel } from './login.model';

export namespace Authentication {

  export interface State {
    isLoggedIn: boolean;
    currentUser: CurrentUserModel;
  }

  export const InitialState: State = {
    isLoggedIn: false,
    currentUser: null
  }


  export class LoginAction implements StateAction<State> {
    static readonly actionType = '[Authentication] Login';
    readonly type = LoginAction.actionType;

    constructor(public login: LoginModel) { }

    reduce(state: State): State {
      return {
        ...InitialState,
      }
    }
  }

  export class LogoutAction implements StateAction<State> {
    static readonly actionType = '[Authentication] Logout'
    readonly type = LogoutAction.actionType;

    constructor() { }

    reduce(state: State): State {
      return {
        ...InitialState
      }
    }
  }

  export class GetCurrentUserAction implements StateAction<State> {
    static readonly actionType = '[Authentication] GetCurrentUser'
    readonly type = GetCurrentUserAction.actionType;

    constructor(public redirectBack: boolean = true) { }

    reduce(state: State): State {
      return state;
    }
  }

  export class SetCurrentUserAction implements StateAction<State> {
    static readonly actionType = '[Authentication] SetCurrentUser'
    readonly type = SetCurrentUserAction.actionType;

    constructor(public user: CurrentUserModel) { }

    reduce(state: State): State {
      return {
        ...state,
        isLoggedIn: true,
        currentUser: this.user
      }
    }
  }


  const supportedActions = [
    LoginAction.actionType,
    LogoutAction.actionType,
    SetCurrentUserAction.actionType,
    GetCurrentUserAction.actionType
  ];

  export function reducer(state = InitialState, action: Action): State {
    if (!supportedActions.includes(action.type)) return state;
    return (action as StateAction<State>).reduce(state);
  }
}
