import { Injectable } from '@angular/core';
import { Location } from '@angular/common';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable } from "rxjs";
import { tap } from 'rxjs/operators';
import { Router } from "@angular/router";
import { AuthenticationRoutes, publicBasePath } from './authentication-routes';

@Injectable()
export class JwtAuthorizationInterceptor implements HttpInterceptor {
  constructor(private router: Router, private location: Location) {

  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    req = req.clone({
      setHeaders: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    });

    return next.handle(req)
      .pipe(
        tap(event => { }
          , error => {
            if (error instanceof HttpErrorResponse) {
              if (error.status == 401) {
                let backUrl = this.location.path();

                const route = '/' + publicBasePath;
                if (backUrl.startsWith(route)) return;

                const queryParams = {};
                if (backUrl && backUrl != '/') queryParams['backUrl'] = backUrl;

                this.router.navigate([AuthenticationRoutes.login], { queryParams });
              }
            }
          })
      );
  }
}
