export const publicBasePath = 'public';

export const loginPath = 'login';

export const forgotPasswordPath = 'forgot-password';

export const resetPasswordPath = 'reset-password';

export class AuthenticationRoutes {
  static resetPassword = '/' + publicBasePath + '/' + resetPasswordPath;
  static forgotPassword = '/' + publicBasePath + '/' + forgotPasswordPath;
  static login = '/' + publicBasePath + '/' + loginPath;
}

