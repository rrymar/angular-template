export class ResetPasswordModel {
  requestId: number;
  secret: string;
  password: string;
}
