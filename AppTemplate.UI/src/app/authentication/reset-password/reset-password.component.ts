import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { ResetPasswordModel } from './reset-password.model';
import { AuthenticationRoutes } from '../authentication-routes';


@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {
  form: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private http: HttpClient) {
  }

  ngOnInit() {
    this.form = this.formBuilder.group({
      password: ['', [Validators.maxLength(32)]]
    });
  }

  onSubmit() {
    if (!this.form.valid) {
      return;
    }

    const id = this.route.snapshot.paramMap.get('id');
    const secret = this.route.snapshot.queryParamMap.get('secret');

    const model: ResetPasswordModel = {
      password: this.form.value.password,
      requestId: Number(id),
      secret: secret
    };
    this.http.post("api/Authentication/ResetPassword", model)
      .subscribe(() => {
        this.toastr.success("Password was changed.");
        this.goBack();
      }, e => {
        this.goBack();
        throw e;
      });
  }

  private goBack() {
    this.router.navigate([AuthenticationRoutes.login]);
  }
}
