import { Actions, Effect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { map, switchMap, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { of, Observable } from 'rxjs';
import { ErrorAction } from '../errors/error.action';
import { Authentication } from './authentication.reducer';
import { TokenModel } from './token.model';
import { UiError } from '../errors/ui-error';
import { Action } from '@ngrx/store';
import { CurrentUserModel } from './current-user.model';
import { AuthenticationRoutes } from './authentication-routes';

@Injectable()
export class AuthenticationEffects {
  constructor(private actions$: Actions,
    private httpClient: HttpClient,
    private router: Router) {
  }

  @Effect()
  login$ = this.actions$.pipe(
    ofType(Authentication.LoginAction.actionType),
    switchMap(a => this.onLogin(a)));

  private onLogin(action: Authentication.LoginAction): Observable<Action> {
    return this.httpClient.post<TokenModel>('api/Authentication/Token', action.login).pipe(
      map(r => {
        localStorage.setItem('token', r.token);
        return new Authentication.GetCurrentUserAction();
      }), catchError(e => {
        if (e instanceof HttpErrorResponse && e.status == 401)
          return of(new ErrorAction(new UiError('Please verify your username and password and try again.')))
        return of(new ErrorAction(e))
      }));
  }

  @Effect()
  getCurrentUser$ = this.actions$.pipe(
    ofType(Authentication.GetCurrentUserAction.actionType),
    switchMap(a => this.onGetCurrentUser(a)));

  private onGetCurrentUser(action: Authentication.GetCurrentUserAction): Observable<Action> {
    return this.httpClient.get<CurrentUserModel>('api/Authentication/CurrentUser').pipe(
      map(r => {
        if (action.redirectBack) {
          const backUrl = this.router.routerState.snapshot.root.queryParamMap.get('backUrl');
          this.router.navigate([backUrl || '']);
        }
        return new Authentication.SetCurrentUserAction(r);
      }), catchError(e => {
        return e instanceof HttpErrorResponse && e.status == 401
          ? of(new ErrorAction(e))
          : of(new ErrorAction(e), new Authentication.LogoutAction());
      })
    );
  }

  @Effect({ dispatch: false })
  logout$ = this.actions$.pipe(
    ofType(Authentication.LogoutAction.actionType),
    map(() => this.onLogout()));

  private onLogout() {
    localStorage.removeItem('token');
    this.router.navigate([AuthenticationRoutes.login]);
  }
}
