export class CurrentUserModel {
  id: number;
  name: string;
  isAdmin: boolean;
}
