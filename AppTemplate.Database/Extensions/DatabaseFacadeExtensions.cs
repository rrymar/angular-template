using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.SqlClient;

namespace AppTemplate.Database.Extensions
{
   public static class DatabaseFacadeExtensions
    {
        public static IList<T> ExecuteSqlCommand<T>(this DatabaseFacade databaseFacade, string sql, Func<DbDataReader, T> getResult, params SqlParameter[] parameters)
        {
            List<T> list = new List<T>();
            databaseFacade.ExecuteReader(sql, reader => list.Add(getResult(reader)), parameters);
            return list;
        }

        public static void ExecuteReader(this DatabaseFacade databaseFacade, string sql, Action<DbDataReader> processRow, params SqlParameter[] parameters)
        {
            var rawSqlCommand = databaseFacade.GetService<IRawSqlCommandBuilder>().Build(sql, parameters);
            using (var reader = rawSqlCommand.RelationalCommand.ExecuteReader(databaseFacade.GetService<IRelationalConnection>(), rawSqlCommand.ParameterValues))
            {
                while (reader.DbDataReader.Read())
                {
                    processRow(reader.DbDataReader);
                }
            }
        }

        public static int ExecuteNonQuery(this DatabaseFacade databaseFacade, string sql, params object[] parameters)
        {
            var rawSqlCommand = databaseFacade.GetService<IRawSqlCommandBuilder>().Build(sql, parameters);
            return rawSqlCommand.RelationalCommand.ExecuteNonQuery(databaseFacade.GetService<IRelationalConnection>(), rawSqlCommand.ParameterValues);
        }
    }
}