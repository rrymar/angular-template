using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace AppTemplate.Database.Extensions
{
    public class QueryParams
    {
        public const int MaxParameterLength = 512;

        public QueryParams(bool trackResult = false, bool hasEntitySet = false)
        {
            TrackResult = trackResult;
            HasEntitySet = hasEntitySet;
        }

        public List<SqlParameter> SqlParameters { get; set; } = new List<SqlParameter>();

        public SqlParameter[] SqlParametersArray => SqlParameters.ToArray();

        public List<KeyValuePair<string, string>> InjectionParameters { get; set; } = new List<KeyValuePair<string, string>>();

        public bool TrackResult { get; set; }

        public bool HasEntitySet { get; set; }

        public void Add<T>(string paramName, T val, Type type = null)
        {
            var paramType = type ?? val?.GetType() ?? typeof(T);
            var value = val == null ? DBNull.Value : (object)val;
            if (paramType == typeof(string))
            {
                var strValue = value;
                if (value.ToString().Length > MaxParameterLength)
                    strValue = value.ToString().Substring(0, MaxParameterLength);

                SqlParameters.Add(new SqlParameter(paramName, SqlDbType.VarChar) { Value = strValue });
            }
            else if (paramType == typeof(int) || paramType == typeof(int?))
                SqlParameters.Add(new SqlParameter(paramName, SqlDbType.Int) { Value = value });
            else if (paramType == typeof(DateTime) || paramType == typeof(DateTime?))
                SqlParameters.Add(new SqlParameter(paramName, SqlDbType.DateTime) { Value = value });
            else if (paramType == typeof(bool) || paramType == typeof(bool?))
                SqlParameters.Add(new SqlParameter(paramName, SqlDbType.Bit) { Value = value });
            else if (paramType == typeof(decimal) || paramType == typeof(decimal?))
                SqlParameters.Add(new SqlParameter(paramName, SqlDbType.Decimal) { Value = value });
            else if (paramType == typeof(Guid) || paramType == typeof(Guid?))
                SqlParameters.Add(new SqlParameter(paramName, SqlDbType.UniqueIdentifier) { Value = value });
            else if (paramType.IsEnum || IsNullableEnum(paramType))
                SqlParameters.Add(new SqlParameter(paramName, SqlDbType.Int) { Value = value });
            else
                throw new ArgumentException($"Type {paramType.Name} is not supported");
        }

        private static bool IsNullableEnum(Type t)
        {
            Type u = Nullable.GetUnderlyingType(t);
            return (u != null) && u.IsEnum;
        }
    }
}