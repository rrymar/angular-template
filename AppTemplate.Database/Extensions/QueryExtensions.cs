using AppTemplate.Database.Entities;
using System.Linq;

namespace AppTemplate.Database.Extensions
{
    public static class QueryExtensions
    {
        public static IQueryable<T> FilterActiveOnly<T>(this IQueryable<T> query, bool activeOnly)
            where T : IDeactivatable
        {
            if (!activeOnly) return query;

            return query.Where(e => e.IsActive);
        }
    }
}
