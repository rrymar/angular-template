using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Metadata;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace AppTemplate.Database.Extensions
{
  public static class DbContextClearExtension
    {
        public static void ClearCache(this DbContext context)
        {
            var stateManager = GetProp<StateManager>(context.ChangeTracker, "StateManager");

            var entityReferenceMap = GetField<Dictionary<object, InternalEntityEntry>>(stateManager, "_entityReferenceMap");
            entityReferenceMap?.Clear();

            var referencedUntrackedEntities = GetField
                <LazyRef<IDictionary<object, IList<Tuple<INavigation, InternalEntityEntry>>>>>(
                    stateManager, "_referencedUntrackedEntities");

            if (referencedUntrackedEntities.HasValue)
                referencedUntrackedEntities.Value.Clear();

            var identityMaps = GetField<Dictionary<IKey, IIdentityMap>>(stateManager, "_identityMaps");
            identityMaps?.Clear();

            ClearIdentityMap(stateManager, "_identityMap0");
            ClearIdentityMap(stateManager, "_identityMap1");

        }

        private static void ClearIdentityMap(StateManager stateManager, string name)
        {
            var obj = GetField<IIdentityMap>(stateManager, name);
            if (obj == null)
                return;

            var identityMap = GetIdentityMap(obj, "_identityMap");
            identityMap?.Clear();

            var dependentMaps = GetIdentityMap(obj, "_dependentMaps");
            dependentMaps?.Clear();

        }

        private static System.Collections.IDictionary GetIdentityMap(IIdentityMap baseObj, string fieldName)
        {
            var map = GetField<System.Collections.IDictionary>(baseObj, fieldName, null, false);
            if (map != null)
                return map;

            if (baseObj is IdentityMap<int>)
                map = GetField<System.Collections.IDictionary>(baseObj, fieldName, typeof(IdentityMap<int>));
            else if (baseObj is IdentityMap<string>)
                map = GetField<System.Collections.IDictionary>(baseObj, fieldName, typeof(IdentityMap<string>));
            else if (baseObj is IdentityMap<object[]>)
                //Composite key
                map = GetField<System.Collections.IDictionary>(baseObj, fieldName, typeof(IdentityMap<object[]>));
            else
                throw new NotImplementedException("Please check and implement it for this case");

            return map;
        }

        private static T GetField<T>(object obj, string name, Type type = null, bool throwIfNotFound = true)
        {
            var field = (type ?? obj.GetType()).GetField(name, BindingFlags.NonPublic | BindingFlags.Instance);
            if (field == null)
            {
                if (throwIfNotFound)
                    throw new InvalidDataException("Field not found");

                return default(T);
            }

            var value = field.GetValue(obj);
            return (T)value;
        }

        private static T GetProp<T>(object obj, string name, Type type = null)
        {
            var prop = (type ?? obj.GetType()).GetProperty(name, BindingFlags.NonPublic | BindingFlags.Instance);
            if (prop == null)
                throw new InvalidDataException("Field not found");

            var value = prop.GetValue(obj);
            return (T)value;
        }
    }
}