namespace AppTemplate.Database.Entities
{
    public interface IDeactivatable
    {
        bool IsActive { get; set; }
    }
}