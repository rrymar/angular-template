﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppTemplate.Database.Entities.Users
{
    public class ResetPasswordRequest : AuditEntity
    {
        public int UserId { get; set; }

        [ForeignKey(nameof(UserId))]
        public User User { get; set; }

        public Guid Secret { get; set; }
    }
}
