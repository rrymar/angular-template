using System.Collections.Generic;
using System.Security.Claims;

namespace Web.Core.User
{
    public interface ICurrentUserContext
    {
        int UserId { get; }

        string GetUserName();

        string GetFullName();

        bool HasRole(string role);

        void Impersonate(IEnumerable<Claim> userClaims);
    }
}