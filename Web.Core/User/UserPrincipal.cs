﻿using System.Collections.Generic;
using System.Security.Claims;

namespace Web.Core.User
{
    public class UserPrincipal
    {
        public string Name { get; set; }

        public List<Claim> Claims { get; set; }

        public UserPrincipal(int userId, string name, string fullName, IEnumerable<string> roles)
        {
            Name = name;
            Claims = new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier, userId.ToString()),
                    new Claim(ClaimTypes.Name, name),
                    new Claim(ClaimsConstants.FullName, fullName),
                };

            foreach (var userRole in roles)
            {
                Claims.Add(new Claim(ClaimTypes.Role, userRole));
            }

        }
    }
}
