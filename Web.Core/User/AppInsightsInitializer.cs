﻿using Microsoft.ApplicationInsights.Channel;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.AspNetCore.Http;

namespace Web.Core.User
{
    public class AppInsightsInitializer : ITelemetryInitializer
    {
        private IHttpContextAccessor httpContextAccessor;
        public AppInsightsInitializer(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }
        public void Initialize(ITelemetry telemetry)
        {
            var userName = httpContextAccessor.HttpContext?.User?.Identity.Name;
            if (!string.IsNullOrEmpty(userName))
            {
                telemetry.Context.User.Id = userName;
            }
        }
    }
}
