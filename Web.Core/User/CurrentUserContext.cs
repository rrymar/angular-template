using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace Web.Core.User
{
    public class CurrentUserContext : ICurrentUserContext
    {
        private readonly IHttpContextAccessor httpContextAccessor;

        public CurrentUserContext(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        private IEnumerable<Claim> claims;

        public IEnumerable<Claim> Claims
        {
            get
            {
                return claims ?? (claims = httpContextAccessor.HttpContext?.User?.Claims);
            }
        }

        private int? userId;

        public int UserId
        {
            get
            {
                return userId ?? (userId = int.Parse(GetClaim(ClaimTypes.NameIdentifier))).Value;
            }
        }

        public string GetUserName()
        {
            return GetClaim(ClaimTypes.Name);
        }

        public string GetFullName()
        {
            return GetClaim(ClaimsConstants.FullName);
        }

        private string GetClaim(string claimType)
        {
            return Claims?.SingleOrDefault(c => c.Type == claimType)?.Value;
        }

        public bool HasRole(string role)
        {
            return Claims.Any(c => c.Type == ClaimTypes.Role && c.Value == role);
        }

        public void Impersonate(IEnumerable<Claim> userClaims)
        {
            claims = userClaims;
        }
    }
}