﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System.Collections;
using System.Collections.Generic;

namespace Web.Core
{
    public static class JsonExtensions
    {
        public static JArray ToJsonArray<T>(this IEnumerable<T> entities)
        {
            return ToJsonArrayImpl(entities);
        }

        public static string ToJsonString(this object entities)
        {
            var enumerable = entities as IEnumerable;
            if (enumerable != null)
                return enumerable.ToJsonArrayImpl().ToString();

            return entities.ToJson().ToString();
        }

        private static JArray ToJsonArrayImpl(this IEnumerable entities)
        {
            var serializer = CreateJsonSerializer();
            return JArray.FromObject(entities, serializer);
        }

        private static JsonSerializer CreateJsonSerializer(IContractResolver resolver = null)
        {
            var settings = new JsonSerializerSettings()
            {
                ContractResolver = resolver,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };
            return JsonSerializer.Create(settings);
        }


        public static JObject ToJson(this object entity)
        {
            var serializer = CreateJsonSerializer();
            return JObject.FromObject(entity, serializer);
        }
    }
}
