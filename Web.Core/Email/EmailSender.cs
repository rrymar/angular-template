﻿using Microsoft.Extensions.Options;
using Scriban;
using System;
using System.Net;
using System.Net.Mail;

namespace Web.Core.Email
{
    public class EmailSender : IEmailSender, IDisposable
    {
        private readonly EmailOptions options;

        private SmtpClient client;

        public EmailSender(IOptions<EmailOptions> options)
        {
            if (options.Value == null)
                throw new ApplicationException("Please specify email options in app.settings.");

            this.options = options.Value;
        }

        public void Send(EmailModel email)
        {
            EnsureClient();

            var parsedTemplate = Template.Parse(email.Template);
            var body = parsedTemplate.Render(email.Model);

            var mailMessage = new MailMessage();
            mailMessage.From = new MailAddress(options.From);
            mailMessage.To.Add(email.ToEmail);
            mailMessage.Body = body;
            mailMessage.Subject = email.Subject;
            client.Send(mailMessage);
        }

        private void EnsureClient()
        {
            if (client != null) return;

            client = new SmtpClient(options.Smtp.Server)
            {
                UseDefaultCredentials = false,
                EnableSsl = options.Smtp.EnableSsl,
                Port = options.Smtp.Port,
                Credentials = new NetworkCredential(options.Smtp.User, options.Smtp.Password)
            };
        }

        public void Dispose()
        {
            client?.Dispose();
        }
    }
}
