﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.Core.Email
{
    public class EmailOptions
    {
        public string UiRoot { get; set; }

        public string From { get; set; }

        public SmtpOptions Smtp { get; set; }
    }

    public class SmtpOptions
    {
        public string Server { get; set; }

        public int Port { get; set; }

        public bool EnableSsl { get; set; }

        public string User { get; set; }

        public string Password { get; set; }
    }
}
