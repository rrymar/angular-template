﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.Core.Email
{
    public class EmailModel
    {
        public string Subject { get; set; }

        public string Template { get; set; }

        public object Model { get; set; }

        public string ToEmail { get; set; }
    }

    public interface IEmailSender
    {
        void Send(EmailModel email);
    }
}
