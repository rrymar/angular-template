using System;
using System.Security.Cryptography;

namespace Web.Core
{
    public class HashService
    {
        public const int SaltSize = 24;
        public const int HashSize = 64;
        public const int Iterations = 30000;

        public (string hash, string salt) CreateHash(string str)
        {
            byte[] salt = new byte[SaltSize];
            using (var provider = new RNGCryptoServiceProvider())
            {
                provider.GetBytes(salt);
            }

            using (var algorithm = new Rfc2898DeriveBytes(str, salt, Iterations))
            {
                var hash = algorithm.GetBytes(HashSize);
                return (Convert.ToBase64String(hash), Convert.ToBase64String(salt));
            }
        }

        public bool ValidateHash(string str, string hash, string salt) {

            var saltBytes =  Convert.FromBase64String(salt);
            using (var algorithm = new Rfc2898DeriveBytes(str, saltBytes, Iterations))
            {
                var newHash = algorithm.GetBytes(HashSize);
                return Convert.ToBase64String(newHash) == hash;
            }
        }
    }
}