using System;
using Microsoft.AspNetCore.Http;

namespace Web.Core.Errors
{
  public static class ErrorLog
    {
        public const int MaxRequestLength = 300;

        public static void Log(Exception ex, string message, string userName, HttpRequest request = null)
        {
            var requestUri = request?.Path.ToString();
            if (requestUri != null && requestUri.Length > MaxRequestLength)
                requestUri = requestUri.Substring(0, MaxRequestLength);

            var template = $"{{RequestUri}} {Environment.NewLine}{message}{Environment.NewLine} {{UserName}}";
            Serilog.Log.Error(ex, template, requestUri, userName);
        }
    }
}