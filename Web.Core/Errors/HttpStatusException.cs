using System;
using System.Net;
using System.Runtime.Serialization;

namespace Web.Core.Errors
{
    public abstract class HttpStatusException : Exception
    {
        public abstract HttpStatusCode Status { get; }

        public HttpStatusException()
        {
        }

        public HttpStatusException(string message) : base(message)
        {
        }

        public HttpStatusException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        protected HttpStatusException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}