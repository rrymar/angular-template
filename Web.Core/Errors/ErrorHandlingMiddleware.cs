using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Web.Core.Errors
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await next.Invoke(context);
            }
            catch (BusinessValidationException e)
            {
                HandleException(context, 422, e.Message);
            }
            catch (DbUpdateConcurrencyException e)
            {
                context.Response.Headers.Add("Location", context.Request.Path.Value);
                HandleException(context, 307, e.Message);
            }
            catch (HttpStatusException e)
            {
                Log.Warning(e, $"{e.GetType().Name} exception occurred");
                HandleException(context, (int)e.Status, e.Message);
            }
            catch (UnauthorizedAccessException e)
            {
                HandleUnauthorizedAccess(context, e);
            }
            catch (AggregateException e)
            {
                LogError(context, e, "Unhandled AggregateException ocurred");
                HandleException(context, 500, e.InnerExceptions.Select(i => i.Message).ToArray());
            }
            catch (Exception e)
            {
                var innerUnauthorized = GetUnauthorizedAccessException(e);
                if (innerUnauthorized != null)
                {
                    HandleUnauthorizedAccess(context, innerUnauthorized);
                    return;
                }

                LogError(context, e, message: "Unhandled exception occurred");
                HandleException(context, 500, e.Message);
            }
        }

        private void LogError(HttpContext context, Exception e, string message)
        {
            var accessor = new HttpContextAccessor() { HttpContext = context };
            var userName = context.User.Claims
                .SingleOrDefault(c => c.Type == ClaimTypes.Name)?.Value;

            ErrorLog.Log(e, message, userName, context?.Request);
        }

        private UnauthorizedAccessException GetUnauthorizedAccessException(Exception e)
        {
            if (e.InnerException is UnauthorizedAccessException innerUnauthorized)
                return innerUnauthorized;

            innerUnauthorized = e.InnerException?.InnerException as UnauthorizedAccessException;

            return innerUnauthorized ?? e.InnerException?.InnerException?.InnerException as UnauthorizedAccessException;
        }

        private void HandleUnauthorizedAccess(HttpContext context, UnauthorizedAccessException e)
        {
            Log.Warning(e, "UnauthorizedAccessException exception occurred");
            HandleException(context, 403);
        }

        private static void HandleException(HttpContext context, int code, params string[] errors)
        {
            if (!context.Response.HasStarted)
            {
                var responseHeaders = new HeaderDictionary();
                foreach (var header in context.Response.Headers)
                    responseHeaders.Add(header);

                context.Response.Clear();

                foreach (var header in responseHeaders)
                    context.Response.Headers.Add(header);
            }

            WriteErrorResponse(context, code, errors);
        }

        private static void WriteErrorResponse(HttpContext context, int code, IEnumerable<string> errors)
        {
            var response = new ErrorResult(errors, context.TraceIdentifier);
            context.Response.StatusCode = code;
            context.Response.ContentType = "application/json";
            context.Response.WriteAsync(JsonConvert.SerializeObject(response, new JsonSerializerSettings
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            }));
        }
    }
}