using System;

namespace Web.Core.Errors
{
    public class BusinessValidationException : Exception
    {
        public BusinessValidationException(string message) : base(message)
        {

        }

        public BusinessValidationException() : base()
        {
        }

        protected BusinessValidationException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context)
        {
        }

        public BusinessValidationException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}