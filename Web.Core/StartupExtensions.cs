using System;
using System.Collections.ObjectModel;
using System.Data;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;
using Serilog.Sinks.MSSqlServer;
using Web.Core.Errors;

namespace Web.Core
{
    public static class StartupExtensions
    {
        public static void AddSerilog(this ILoggerFactory loggerFactory, IConfiguration configuration,
            string connectionStringName)
        {
            var minimumLevel = (LogEventLevel)Enum.Parse(typeof(LogEventLevel), configuration["Serilog:MinimumLevel"]);
            var connectionString = configuration.GetConnectionString(connectionStringName);

            var sqlOptions = new ColumnOptions();
            sqlOptions.Store.Remove(StandardColumn.MessageTemplate);
            sqlOptions.Store.Remove(StandardColumn.Properties);
            sqlOptions.AdditionalDataColumns = new Collection<DataColumn>
            {
                new DataColumn("RequestUri", typeof(string)) { MaxLength = ErrorLog.MaxRequestLength },
                new DataColumn("UserName",typeof(string)) { MaxLength = 50 },
            };

            Log.Logger = new LoggerConfiguration().MinimumLevel.Debug()
                .WriteTo.RollingFile("Logs\\log-{Date}.txt", retainedFileCountLimit: 10,
                    restrictedToMinimumLevel: minimumLevel)
                .WriteTo.MSSqlServer(connectionString, "Errors", LogEventLevel.Error,
                    columnOptions: sqlOptions)
                .CreateLogger();

            loggerFactory.AddSerilog();
        }

        public static IConfigurationRoot AddAppSettings(this IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                 .SetBasePath(env.ContentRootPath)
                 .AddJsonFile("appsettings.json");

            builder.AddEnvironmentVariables();
            builder.AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);
            return builder.Build();
        }

        public static void AddDatabase<T>(this IServiceCollection services,
            string connectionString, string migrationsAssembly)
            where T : DbContext
        {
            services.AddDbContext<T>(options =>
            {
                options.UseSqlServer(connectionString,
                    x => x.MigrationsAssembly(migrationsAssembly));

                options.ConfigureWarnings(w => w.Throw(RelationalEventId.QueryClientEvaluationWarning));
            });
        }

        public static ContainerBuilder AddAutofac(this IServiceCollection services)
        {
            var builder = new ContainerBuilder();
            builder.Populate(services);

            return builder;
        }
    }
}