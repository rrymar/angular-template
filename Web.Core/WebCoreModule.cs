using Autofac;
using Web.Core.Email;
using Web.Core.User;

namespace Web.Core
{
    public class WebCoreModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<HashService>().AsSelf();
            builder.RegisterType<CurrentUserContext>().As<ICurrentUserContext>()
                .InstancePerLifetimeScope();

            builder.RegisterType<EmailSender>().As<IEmailSender>();
        }
    }
}