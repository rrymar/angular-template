﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.Core.CRUD
{
    public class ResultsList<T>
    {
        public int TotalCount { get; set; }

        public List<T> Items { get; set; }

        public ResultsList(List<T> items, int totalCount)
        {
            TotalCount = totalCount;
            Items = items;
        }

        public ResultsList()
        {
        }
    }
}
