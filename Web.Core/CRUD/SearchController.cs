﻿using Microsoft.AspNetCore.Mvc;

namespace Web.Core.CRUD
{
    public interface ISearchService<T, TQuery>
    {
        ResultsList<T> Get(TQuery query);
    }

    public abstract class SearchController<T, TQuery> : Controller
    {
        protected readonly ISearchService<T, TQuery> service;

        protected SearchController(ISearchService<T, TQuery> service)
        {
            this.service = service;
        }

        [HttpGet]
        public ResultsList<T> Get([FromQuery] TQuery query)
        {
            return service.Get(query);
        }
    }
}
