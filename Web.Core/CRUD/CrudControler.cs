﻿using Microsoft.AspNetCore.Mvc;

namespace Web.Core.CRUD
{
    public interface ICrudService<T, TQuery> : ISearchService<T, TQuery>
    {
        T Get(int id);
        T Create(T model);
        T Update(int id, T model);
        void Delete(int id);
    }

    public abstract class CrudControler<T, TQuery> : SearchController<T, TQuery>
    {
        protected ICrudService<T, TQuery> Service => (ICrudService<T, TQuery>)service;

        protected CrudControler(ICrudService<T, TQuery> service) : base(service)
        {
        }

        [HttpGet("{id}")]
        public T Get(int id)
        {
            return Service.Get(id);
        }

        [HttpPost]
        public T Create([FromBody] T user)
        {
            return Service.Create(user);
        }

        [HttpPut("{id}")]
        public T Update(int id, [FromBody] T user)
        {
            return Service.Update(id, user);
        }

        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            Service.Delete(id);
        }
    }
}
