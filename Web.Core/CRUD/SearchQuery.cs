﻿namespace Web.Core.CRUD
{
    public class SearchQuery
    {
        public string Keyword { get; set; }

        public int PageIndex { get; set; }

        public int PageSize { get; set; } = 20;

        public string SortField { get; set; }

        public bool IsDesc { get; set; }

        public bool ActiveOnly { get; set; }
    }
}
