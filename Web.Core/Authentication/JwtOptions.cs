﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Web.Core.Authentication
{
    public class JwtOptions
    {
        public string Key { get; set; }

        public string Issuer { get; set; }

        public int ExpireInHours { get; set; }
    }
}
