﻿using Tests.Core.TestApp;
using Tests.Core.TestClient;
using Xunit;

namespace AppTemplate.Web.Tests.Core
{
    public class SwaggerTests : IntegrationTest
    {
        public SwaggerTests(TestApplication<Fixture> fixture) : base(fixture)
        {
        }

        [Fact]
        public void ItReturnsSwaggerSchema()
        {
            TestClient.Get("swagger/v1/swagger.json".ToRestRequest());
        }
    }
}
