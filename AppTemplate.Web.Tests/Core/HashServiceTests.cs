using FluentAssertions;
using Web.Core;
using Xunit;

namespace AppTemplate.Web.Tests.Core
{
    public class HashServiceTests
    {
        [Fact]
        public void ItGenerateHash()
        {
            var hashService = new HashService();
            var result = hashService.CreateHash("1234");
            result.hash.Should().NotBeNullOrEmpty();
            result.salt.Should().NotBeNullOrEmpty();

            hashService.ValidateHash("1234", result.hash, result.salt).Should().BeTrue();
            hashService.ValidateHash("12345", result.hash, result.salt).Should().BeFalse();
        }
    }
}
