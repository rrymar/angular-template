﻿using AppTemplate.Database.Entities.Users;
using database.migrations;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Tests.Core.Db;
using Tests.Core.TestApp;
using Xunit;

namespace AppTemplate.Web.Tests.Database
{
    public class MigrationsTests : IntegrationTest
    {
        public MigrationsTests(TestApplication<Fixture> fixture) : base(fixture)
        {
        }

        [Fact]
        public void IsMigrationGenerated()
        {
            Context.IsMigrationGenerated();
        }

        [Fact]
        public void IsMigrationFileNamedAsMigrationId()
        {
            Context.IsMigrationFileNamedAsMigrationId(Migrations.Assembly);
        }

        [Fact]
        public void ItClearContext()
        {
            var user = Context.Users.Include(u => u.Roles)
                .Single(u => u.Id == KnownUsers.Admin);

            Context.ClearCache();

            var actual = Context.Users.Single(u => u.Id == KnownUsers.Admin);
            actual.Roles.Should().BeNull();
        }
    }
}
