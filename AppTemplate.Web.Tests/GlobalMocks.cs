﻿using Autofac;
using Moq;
using Web.Core.Email;

namespace AppTemplate.Web.Tests
{
    public class GlobalMocks
    {
        public Mock<IEmailSender> EmailSender { get; }

        public GlobalMocks()
        {
            EmailSender = new Mock<IEmailSender>();
        }
    }

    public static class GlobalMocksExtensions
    {
        public static void RegisterTestMocks(this ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterType<GlobalMocks>().AsSelf()
                .InstancePerLifetimeScope();

            containerBuilder.Register(c => c.Resolve<GlobalMocks>().EmailSender.Object)
                .As<IEmailSender>();
        }
    }
}
