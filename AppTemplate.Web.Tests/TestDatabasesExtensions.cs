using AppTemplate.Database;
using AppTemplate.Web.Tests.TestMigrations;
using Autofac;
using database.migrations;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Tests.Core.Db;

namespace AppTemplate.Web.Tests
{
    public static class TestDatabasesExtensions
    {
        private static readonly object locker = new object();

        private static bool dbInitialized;

        public static void InitTestDatabases(this IApplicationBuilder app)
        {
            var scope = app.ApplicationServices.GetRequiredService<ILifetimeScope>();

            lock (locker)
            {
                if (dbInitialized) return;

                var db = new TestDatabaseMigrator(scope.Resolve<DataContext>());
                db.Create();
                db.RunMigrations(Migrations.Assembly);
                db.RunMigrations(TestMigrationsList.Migrations);

                dbInitialized = true;
            }
        }
    }
}
