using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Reflection;
using Tests.Core.TestApp;
using Xunit;

namespace AppTemplate.Web.Tests
{
    public class Fixture : Startup
    {
        public Fixture(IHostingEnvironment env) : base(env)
        {
            FixtureScopeFactory.FixtureScope = null;
        }

        public override void RegisterServices(ContainerBuilder builder)
        {
            builder.RegisterTestMocks();
        }

        public override void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            base.Configure(app, env, loggerFactory);
            app.InitTestDatabases();
        }

        protected override IMvcBuilder AddMvc(IServiceCollection services)
        {
            return base.AddMvc(services).AddApplicationPart(Assembly.Load(new AssemblyName("AppTemplate.Web")));
        }
    }

    [CollectionDefinition(Name)]
    public class FixtureCollection : ICollectionFixture<TestApplication<Fixture>>
    {
        public const string Name = "FixtureCollection";
    }
}