﻿using AppTemplate.Web.Features.Authentication.CurrentUser;
using AppTemplate.Web.Tests.TestMigrations;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Text;
using Tests.Core.TestApp;
using Tests.Core.TestClient;
using Xunit;

namespace AppTemplate.Web.Tests.Features.Authentication
{
    public class CurrentUserTests : IntegrationTest
    {
        private readonly RestRequest request;

        public CurrentUserTests(TestApplication<Fixture> fixture) : base(fixture)
        {
            request = new RestRequest(Urls.Authentication.CurrentUser);
        }

        [Fact]
        public void ItReturnsAdminAsCurrentUser()
        {
            LoginAs(TestConstants.Admin);
            var result = TestClient.Get<CurrentUserModel>(request);
            result.Id.Should().Be(TestConstants.Admin.Id);
            result.IsAdmin.Should().BeTrue();
        }

        [Fact]
        public void ItReturnsCurrentUser()
        {
            LoginAs(TestConstants.User);
            var result = TestClient.Get<CurrentUserModel>(request);
            result.Id.Should().Be(TestConstants.User.Id);
            result.IsAdmin.Should().BeFalse();
        }
    }
}
