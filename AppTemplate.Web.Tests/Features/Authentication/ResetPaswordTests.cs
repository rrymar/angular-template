﻿using AppTemplate.Web.Features.Authentication.ResetPassword;
using AppTemplate.Web.Tests.TestMigrations;
using Autofac;
using FluentAssertions;
using Moq;
using System.Linq;
using Tests.Core.TestApp;
using Tests.Core.TestClient;
using Web.Core;
using Web.Core.Email;
using Xunit;

namespace AppTemplate.Web.Tests.Features.Authentication
{
    public class ResetPaswordRequest : IntegrationTest
    {
        private readonly RestRequest createRequest;
        private readonly RestRequest resetRequest;

        public ResetPaswordRequest(TestApplication<Fixture> fixture) : base(fixture)
        {
            createRequest = new RestRequest(Urls.Authentication.ResetPasswordRequest);
            resetRequest = new RestRequest(Urls.Authentication.ResetPassword);
        }

        [Fact]
        public void ItResetPassword()
        {
            var hashService = Scope.Resolve<HashService>();

            Mocks.EmailSender.Setup(e => e.Send(It.IsAny<EmailModel>()))
                .Callback<EmailModel>(e =>
                {
                    e.ToEmail.Should().NotBeNullOrEmpty();
                    e.Model.Should().NotBeNull();
                });

            createRequest.AddJsonContent(new ResetPasswordRequestModel()
            {
                Username = TestConstants.User.Username
            });

            TestClient.Post(createRequest);

            Mocks.EmailSender.Verify(e => e.Send(It.IsAny<EmailModel>()), Times.Once());

            var request = Context.ResetPasswordRequests
                .OrderByDescending(e => e.Id).FirstOrDefault();

            request.Should().NotBeNull();
            request.IsActive.Should().BeTrue();
            request.UserId.Should().Be(TestConstants.User.Id);

            var resetModel = new ResetPasswordModel()
            {
                RequestId = request.Id,
                Secret = request.Secret,
                Password = "new pass 123"
            };

            resetRequest.AddJsonContent(resetModel);
            TestClient.Post(resetRequest);

            var user = Context.Users.Find(request.UserId);
            hashService.ValidateHash(resetModel.Password, user.PasswordHash, user.PasswordSalt)
                .Should().BeTrue();

            var updatedRequest = Context.ResetPasswordRequests.Find(request.Id);
            updatedRequest.IsActive.Should().BeFalse();
        }

        [Fact]
        public void ItCantResetPasswordInactive()
        {
            createRequest.AddJsonContent(new ResetPasswordRequestModel()
            {
                Username = TestConstants.InactiveUser.Username
            });

            TestClient.Post(createRequest);

            var request = Context.ResetPasswordRequests
               .OrderByDescending(e => e.Id).FirstOrDefault();

            request.Should().BeNull();
        }
    }
}
