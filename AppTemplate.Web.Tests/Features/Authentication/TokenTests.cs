﻿using AppTemplate.Web.Features.Authentication.Token;
using AppTemplate.Web.Tests.TestMigrations;
using FluentAssertions;
using System.Net;
using Tests.Core.TestApp;
using Tests.Core.TestClient;
using Xunit;
using Tests.Core.TestHttpClient;

namespace AppTemplate.Web.Tests.Features.Authentication
{
    public class TokenTests : IntegrationTest
    {
        private readonly RestRequest tokenRequest;

        public TokenTests(TestApplication<Fixture> fixture) : base(fixture)
        {
            tokenRequest = new RestRequest(Urls.Authentication.Token);
        }

        [Fact]
        public void ItGeneratesToken()
        {
            tokenRequest.AddJsonContent(new LoginModel()
            {
                Username = TestConstants.Admin.Username,
                Password = TestConstants.Admin.Password,
            });

            var result = TestClient.Post<TokenModel>(tokenRequest);

            result.Token.Should().NotBeNull();
        }

        [Fact]
        public void ItVerifiesPassword()
        {
            tokenRequest.AddJsonContent(new LoginModel()
            {
                Username = TestConstants.Admin.Username,
                Password = "xxxxx",
            });

            TestClient.Invoking(t => t.Post<TokenModel>(tokenRequest))
                .ShouldThrowHttpError(HttpStatusCode.Unauthorized);
        }

        [Fact]
        public void ItVerifiesIsActive()
        {
            tokenRequest.AddJsonContent(new LoginModel()
            {
                Username = TestConstants.InactiveUser.Username,
                Password = TestConstants.InactiveUser.Password,
            });

            TestClient.Invoking(t => t.Post<TokenModel>(tokenRequest))
                .ShouldThrowHttpError(HttpStatusCode.Unauthorized);
        }
    }
}
