﻿using AppTemplate.Web.Features.Users;
using AppTemplate.Web.Tests.TestMigrations;
using Autofac;
using FluentAssertions;
using Tests.Core.TestApp;
using Tests.Core.TestClient;
using Tests.Core.TestHttpClient;
using Web.Core;
using Web.Core.CRUD;
using Xunit;

namespace AppTemplate.Web.Tests.Features.Users
{
    public class UsersTests : IntegrationTest
    {
        private readonly RestRequest usersRequest;

        public UsersTests(TestApplication<Fixture> fixture) : base(fixture)
        {
            usersRequest = new RestRequest(Urls.Users);
            LoginAs(TestConstants.Admin);
        }

        [Fact]
        public void ItReturnUserById()
        {
            usersRequest.AddUrlSegment(TestConstants.User.Id);

            var user = TestClient.Get<UserModel>(usersRequest);
            user.Should().NotBeNull();
            user.Username.Should().Be(TestConstants.User.Username);
            user.Password.Should().BeNullOrEmpty();
        }

        [Fact]
        public void ItReturnsUsers()
        {
            var query = new SearchQuery()
            {
                ActiveOnly = false,
                PageIndex = 0,
                PageSize = 2,
                SortField = "id",
                IsDesc = true
            };

            usersRequest.AddQueryToUrl(query);

            var results = TestClient.Get<ResultsList<UserModel>>(usersRequest);

            results.TotalCount.Should().Be(3);
            results.Items.Should().HaveCount(2);

            results.Items[0].Id.Should().BeGreaterThan(results.Items[1].Id);
        }

        [Fact]
        public void ItUpdatesUser()
        {
            var hashService = Scope.Resolve<HashService>();

            usersRequest.AddUrlSegment(TestConstants.User.Id);
            var user = TestClient.Get<UserModel>(usersRequest);
            user.Password = "some pass";
            user.FirstName = "new Name";
            user.Email = "some@email.com";

            usersRequest.AddJsonContent(user);
            var updated = TestClient.Put<UserModel>(usersRequest);
            updated.Should().NotBeNull();
            updated.FullName.Should().Contain(user.FirstName);
            updated.Email.Should().Be(user.Email);

            var dbEntity = Context.Users.Find(user.Id);
            dbEntity.IsActive.Should().BeTrue();
            dbEntity.UpdatedOn.Should().BeAfter(dbEntity.CreatedOn);
            hashService.ValidateHash(user.Password, dbEntity.PasswordHash, dbEntity.PasswordSalt)
                .Should().BeTrue();
        }

        [Fact]
        public void ItCreatesUser()
        {
            var hashService = Scope.Resolve<HashService>();

            var user = new UserModel()
            {
                FirstName = "Lucas",
                LastName = "Djeron",
                Username = "lg25",
                Password = "00000",
                Email = "some@email.com"
            };

            usersRequest.AddJsonContent(user);

            var created = TestClient.Post<UserModel>(usersRequest);
            created.Id.Should().NotBe(default);
            created.Username.Should().Be(user.Username);
            created.FirstName.Should().Be(user.FirstName);
            created.LastName.Should().Be(user.LastName);
            created.Email.Should().Be(user.Email);


            var dbEntity = Context.Users.Find(created.Id);
            hashService.ValidateHash(user.Password, dbEntity.PasswordHash, dbEntity.PasswordSalt)
                .Should().BeTrue();
        }

        [Fact]
        public void ItRequirePasswordForUser()
        {
            var user = new UserModel()
            {
                FirstName = "Lucas",
                LastName = "Djeron",
                Username = "lg25",
                Email = "some@email.com",
            };

            usersRequest.AddJsonContent(user);
            TestClient.Invoking(t => t.Post<UserModel>(usersRequest))
                .ShouldThrowValidationError();
        }

        [Fact]
        public void ItRequireEmailForUser()
        {
            var user = new UserModel()
            {
                FirstName = "Lucas",
                LastName = "Djeron",
                Username = "lg25",
                Password = "00000",
            };

            usersRequest.AddJsonContent(user);
            TestClient.Invoking(t => t.Post<UserModel>(usersRequest))
                .ShouldThrowValidationError();
        }

        [Fact]
        public void ItDeletesUser()
        {
            usersRequest.AddUrlSegment(TestConstants.User.Id);
            TestClient.Delete(usersRequest);

            var dbEntity = Context.Users.Find(TestConstants.User.Id);
            dbEntity.IsActive.Should().BeFalse();
            dbEntity.UpdatedOn.Should().BeAfter(dbEntity.CreatedOn);
        }
    }
}
