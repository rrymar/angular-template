﻿namespace AppTemplate.Web.Tests.TestMigrations
{
    public static class TestConstants
    {
        public static readonly TestUser Admin = new TestUser()
        {
            Id = 1,
            Username = "admin",
            Password = "1234"
        };

        public static readonly TestUser User = new TestUser()
        {
            Id = 2,
            Username = "user",
            Password = "1234567"
        };

        public static readonly TestUser InactiveUser = new TestUser()
        {
            Id = 3,
            Username = "ina",
            Password = "0000"
        };
    }


    public class TestUser
    {
        public int Id { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
