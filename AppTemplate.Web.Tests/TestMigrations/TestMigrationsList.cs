﻿using AppTemplate.Database;
using System;
using System.Collections.Generic;
using System.Text;
using Tests.Core.Db;

namespace AppTemplate.Web.Tests.TestMigrations
{
    public static class TestMigrationsList
    {
        public static readonly List<ITestMigration<DataContext>> Migrations = new List<ITestMigration<DataContext>>
        {
            new AddTestUsers()
        };
    }
}
