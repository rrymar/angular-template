﻿using AppTemplate.Database;
using AppTemplate.Database.Entities.Users;
using Tests.Core.Db;
using Web.Core;

namespace AppTemplate.Web.Tests.TestMigrations
{
    public class AddTestUsers : ITestMigration<DataContext>
    {
        private static readonly HashService hashService = new HashService();

        public string Name => "201907011015_AddTestUsers";

        public void Execute(DataContext context)
        {
            var hashResult = hashService.CreateHash(TestConstants.User.Password);

            var user = new User()
            {
                Username = TestConstants.User.Username,
                PasswordHash = hashResult.hash,
                PasswordSalt = hashResult.salt,
                Email = "active@user.email"
            };

            var inactiveHashResult = hashService.CreateHash(TestConstants.User.Password);


            var inactiveUser = new User()
            {
                Username = TestConstants.InactiveUser.Username,
                PasswordHash = inactiveHashResult.hash,
                PasswordSalt = inactiveHashResult.salt,
                IsActive = false,
                Email = "ina@user.email"
            };

            context.Users.Add(user);
            context.Users.Add(inactiveUser);

            context.SaveChanges();
        }
    }
}
