﻿using AppTemplate.Database;
using AppTemplate.Web.Features.Authentication;
using AppTemplate.Web.Tests.TestMigrations;
using Autofac;
using Microsoft.EntityFrameworkCore;
using Tests.Core;
using Tests.Core.TestApp;
using Web.Core.User;
using Xunit;

namespace AppTemplate.Web.Tests
{
    [Collection(FixtureCollection.Name)]
    public abstract class IntegrationTest : IntegrationTest<Fixture>
    {
        protected GlobalMocks Mocks => Scope.Resolve<GlobalMocks>();

        protected override DbContext Database => Scope.Resolve<DataContext>();

        protected DataContext Context { get; }

        private readonly AuthenticationService authenticationService;

        private readonly ICurrentUserContext currentUserContext;

        protected IntegrationTest(TestApplication<Fixture> fixture) : base(fixture)
        {
            Context = Scope.Resolve<DataContext>();
            authenticationService = Scope.Resolve<AuthenticationService>();
            currentUserContext = Scope.Resolve<ICurrentUserContext>();
        }

        public void LoginAs(TestUser user)
        {
            var userPrincipal = authenticationService.Authenticate(user.Username, user.Password);
            var token = authenticationService.BuildToken(userPrincipal);

            TestClient.AddJwtToken(token);

            // to allow changing user in scope of one test
            currentUserContext.Impersonate(userPrincipal.Claims);
        }
    }
}
